<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

if ( ! function_exists('menu_1'))
{
  function menu_1($menu)
  {
    $ci=& get_instance();

    $segment =  $ci->uri->segment(1);

    if (in_array($segment, $menu))
    {
      echo "active";
    }
  }

  function menu_2($menu)
  {
    $ci=& get_instance();

    $segment =  $ci->uri->segment(1);

    if (in_array($segment, $menu))
    {
      return TRUE;
    }
    else
    {
      return FALSE;
    }
  }

  function menu_3($menu)
  {
    $ci=& get_instance();
    $segment =  $ci->uri->segment(2);

    if (in_array($segment, $menu))
    {
      echo "active";
    }
  }

  function menu_4($menu)
  {
    $ci=& get_instance();
    $segment =  $ci->uri->segment(3);

    if (in_array($segment, $menu))
    {
      echo "selected";
    }
  }

  function menubar($menu)
  {
    $ci=& get_instance();
    $segment =  $ci->uri->segment(2);

    if (in_array($segment, $menu))
    {
      echo "selected";
    }
  }

  function menubar2($menu)
  {
    $ci=& get_instance();
    $segment =  $ci->uri->segment(3);

    if (in_array($segment, $menu))
    {
      echo "black-selected";
    }
  }
}