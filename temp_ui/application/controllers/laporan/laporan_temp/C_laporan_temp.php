<?php
defined('BASEPATH') or exit('No direct script access allowed');

// require_once FCPATH . "assets/vendor/office/autoload.php";
require_once FCPATH . "assets/vendor/box/spout/src/Spout/Autoloader/autoload.php";

date_default_timezone_set('Asia/Jakarta');

use \GuzzleHttp\Client;

use Box\Spout\Writer\Common\Creator\WriterEntityFactory;
use Box\Spout\Writer\Common\Creator\Style\StyleBuilder;
use Box\Spout\Common\Entity\Style\CellAlignment;
use Box\Spout\Common\Entity\Style\Color;
use Box\Spout\Writer\Common\Creator\Style\BorderBuilder;
use Box\Spout\Common\Entity\Style\Border;

class C_laporan_temp extends MY_Controller
{

	public function __construct()
	{
		parent::__construct();

		/*
		Tidak perlu ada yang ditambahkan di construct.
		Sesuaikan dengan instruksi. Untuk perubahan hanya di variable
		$this->form_header.
		*/
		$this->class = str_replace("c_", "", $this->router->fetch_class());
		$this->form_header = 873;

		$this->_rs_key = "900614f7-7acd-11e8-a953-fa163e101f72";

		$token = "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJuYW1lIjoiTkFNQSAyIiwiaWRfdXNlciI6IjM3MzY4MSIsInJtX251bWJlciI6ImFkbWluIiwicnNfa2V5IjoiQTEyMyIsImlwX2FkZHJlc3MiOiIxMjcuMC4xLjEiLCJhY2Nlc3MiOiJ1c2VyIn0.ubW6fyc7ErYOW2T5qFbjXvLIVTLp05s3A0paQ6wfcmo";

		$this->_ci_laporan = new Client([
			'base_uri'  => $this->config->item('api_laporan'),
			'headers'   => [
				'Content-Type' => 'application/json',
				'x-token' => $token
			]
		]);

		$this->_client_ref = new Client([
			'base_uri'  => $this->config->item('api_ref'),
			'headers'   => [
				'Content-Type' => 'application/json',
				'x-token' => $token
			]
		]);

		$this->_client_rs = new Client([
			'base_uri'  => $this->config->item('api_rs'),
			'headers'   => [
				'Content-Type' => 'application/json',
				'x-token' => $token
			]
		]);
	}

	function index()
	{
		$data['title'] = $this->getTitle();
		$data['url'] = base_url($this->class);
		$data['filter'] = $this->getStatistik(date("Y-m-d"), date("Y-m-d"), 1);;
		$data['contents'] = "contents/laporan/{$this->class}/index";
		$this->load->view('master', $data);
	}

	/*
	Function untuk memproses data untuk ditampilkan di datatable
	*/
	function json()
	{

		$value = $this->_postvalue();

		$json = [
			'draw' => $this->input->post('draw'),
			'recordsTotal' => $value['total_data']['total_data'],
			'recordsFiltered' => $value['total_data']['total_data']
		];

		$data = array();

		/*
		Silahkan ubah disini. Sesuaikan dengan tugas masing2
		*/
		$i = $_POST['start'];
		foreach ($value['data'] as $key => $val) {
			$i++;
			$row = array();
			$data_pasien = $val['data_pasien'];
			$data_registrasi = $val['data_registrasi'];
			$data_catatan = $val['data_catatan'];

			$row[] = $i;
			$row[] = $data_pasien['no_rm'];
			$row[] = $data_pasien['no_bpjs'];
			$row[] = $data_pasien['nama_pasien'];
			$row[] = $data_pasien['kelamin'];
			$row[] = $data_pasien['penjamin'];
			$row[] = $data_pasien['umur'];
			$row[] = $data_pasien['alamat_1'] . ', ' . $data_pasien['alamat_2'];
			$row[] = $data_pasien['status'];
			$row[] = $data_pasien['kunjungan_ke'];
			$row[] = $data_registrasi['no_reg'];
			$row[] = $data_registrasi['asal_pasien'];
			$row[] = $data_registrasi['nama_dpjp'];
			$row[] = $data_registrasi['tgl_checkin'];
			$row[] = $data_registrasi['jenis_rawat'];
			$row[] = $data_registrasi['ruang_rawat'];

			foreach ($data_catatan['icd_10_awal'] as $k => $v) {
				$icd_10_awal = $k;
			}
			$row[] = $icd_10_awal;
			$data[] = $row;
		}

		$json['data'] = $data;

		echo json_encode($json);
	}

	/*
	Function untuk mendapatkan nama dan kode form.
	Tidak ada yang perlu diubah disini.
	*/
	private function getTitle()
	{
		$id_form = $this->form_header;

		$form = $this->_client_ref->request('GET', 'form', [
			'query' => ['id' => $id_form]
		]);

		$form = json_decode($form->getbody()->getcontents(), true);
		$form = $form['data'];

		return $form['nama_form'] . ' - ' . $form['kode_form']; // title
	}

	/*
	Function untuk me-request ke endpoint 
	'laporan_temp/statistik_registrasi'
	*/
	private function getStatistik($date_start, $date_end, $jenis_periode)
	{
		$param = [
			'query' => [
				'is_reg_del_date_null' => 1,
				'jenis_periode' => 1,
				'periode_start' => $date_start,
				'periode_end' => $date_end,
				'jenis_rawat' => "RI"
			]
		];

		$request_value = $this->_ci_laporan->request('GET', 'laporan_temp/statistik_registrasi', $param);

		$value = json_decode($request_value->getBody()->getContents(), true);

		return $value['data'];
	}

	function filter()
	{
		$date_start = $this->input->get('tgl_start');
		$akhir = $this->input->get('tgl_end');
		$tanggal = explode('-', $akhir);
		if ($tanggal[2] == '30') {
			$date_end = date('Y-m-t', strtotime($this->input->get('tgl_end')));
		} else {
			$date_end = $this->input->get('tgl_end');
		}
		echo json_encode($this->getStatistik($date_start, $date_end, 1));
	}

	function _getvalue($paging = TRUE)
	{
		$date_start = $this->input->get('tgl_start');
		$akhir = $this->input->get('tgl_end');
		$tanggal = explode('-', $akhir);
		if ($tanggal[2] == '30') {
			$date_end = date('Y-m-t', strtotime($this->input->get('tgl_end')));
		} else {
			$date_end = $this->input->get('tgl_end');
		}

		$param = [
			'query' => [
				'jenis_visit' 			=> "RI",
				'jenis_periode'			=> 1,
				'periode_start' 		=> $date_start,
				'periode_end' 			=> $date_end,
				'is_reg_del_date_null' 	=> 1,
				'shift' 				=> $this->input->get('shift'),
				'is_pasien_baru' 		=> $this->input->get('is_pasien_baru'),
				'gender' 				=> $this->input->get('gender'),
				'umur_start' 			=> $this->input->get('umur_start'),
				'umur_end' 				=> $this->input->get('umur_end'),
				'offset' 				=> $this->input->get('start'),
				'limit' 				=> $this->input->get('length'),
				'search' 				=> $this->input->get('search')['value'],
				'id_ref_payment' 		=> $this->input->get('id_ref_payment'),
				'id_dpjp'				=> $this->input->get('id_dpjp'),
				'id_kec' 				=> $this->input->get('id_kec'),
				'id_kel' 				=> $this->input->get('id_kel'),
				'id_dept_ruang_rawat' 	=> $this->input->get('id_dept_ruang_rawat')
			]
		];

		if (!$this->input->get('shift')) {
			unset($param['query']['shift']);
		}
		if ($this->input->get('is_pasien_baru') == null) {
			unset($param['query']['is_pasien_baru']);
		}
		if (!$this->input->get('gender')) {
			unset($param['query']['gender']);
		}
		if (!$this->input->get('id_dept_ruang_rawat')) {
			unset($param['query']['id_dept_ruang_rawat']);
		}
		if (!$this->input->get('id_ref_payment')) {
			unset($param['query']['id_ref_payment']);
		}
		if (!$this->input->get('id_dpjp')) {
			unset($param['query']['id_dpjp']);
		}
		if (!$this->input->get('id_kec')) {
			unset($param['query']['id_kec']);
		}
		if (!$this->input->get('id_kel')) {
			unset($param['query']['id_kel']);
		}
		if (!$this->input->get('umur_start') or !$this->input->get('umur_end')) {
			unset($param['query']['umur_start']);
			unset($param['query']['umur_end']);
		}
		if (!$paging) {
			unset($param['query']['offset']);
			unset($param['query']['limit']);
		}

		$request_value = $this->_ci_laporan->request('GET', 'laporan_temp', $param);


		$value = json_decode($request_value->getBody()->getContents(), true);

		return $value;
	}

	function _postvalue($paging = TRUE)
	{
		$date_start = $this->input->post('tgl_start');
		$akhir = $this->input->post('tgl_end');
		$tanggal = explode('-', $akhir);
		if ($tanggal[2] == '30') {
			$date_end = date('Y-m-t', strtotime($this->input->post('tgl_end')));
		} else {
			$date_end = $this->input->post('tgl_end');
		}

		$param = [
			'query' => [
				'jenis_visit' 					=> "RI",
				'jenis_periode' 				=> 1,
				'periode_start' 				=> $date_start,
				'periode_end' 					=> $date_end,
				'is_reg_del_date_null' 	=> 1,
				'shift' 								=> $this->input->post('shift'),
				'is_pasien_baru' 				=> $this->input->post('is_pasien_baru'),
				'gender' 								=> $this->input->post('gender'),
				'umur_start' 						=> $this->input->post('umur_start'),
				'umur_end' 							=> $this->input->post('umur_end'),
				'offset' 								=> $this->input->post('start'),
				'limit' 								=> $this->input->post('length'),
				'search' 								=> $this->input->post('search')['value'],
				'id_ref_payment' 				=> $this->input->post('id_ref_payment'),
				'id_dpjp' 							=> $this->input->post('id_dpjp'),
				'id_kec' 								=> $this->input->post('id_kec'),
				'id_kel' 								=> $this->input->post('id_kel'),
				'id_dept_ruang_rawat' 	=> $this->input->post('id_dept_ruang_rawat')
			]
		];

		if (!$this->input->post('shift')) {
			unset($param['query']['shift']);
		}
		if ($this->input->post('is_pasien_baru') == null) {
			unset($param['query']['is_pasien_baru']);
		}
		if (!$this->input->post('gender')) {
			unset($param['query']['gender']);
		}
		if (!$this->input->post('id_dept_ruang_rawat')) {
			unset($param['query']['id_dept_ruang_rawat']);
		}
		if (!$this->input->post('id_ref_payment')) {
			unset($param['query']['id_ref_payment']);
		}
		if (!$this->input->post('id_dpjp')) {
			unset($param['query']['id_dpjp']);
		}
		if (!$this->input->post('id_kec')) {
			unset($param['query']['id_kec']);
		}
		if (!$this->input->post('id_kel')) {
			unset($param['query']['id_kel']);
		}
		if (!$this->input->post('umur_start') or !$this->input->post('umur_end')) {
			unset($param['query']['umur_start']);
			unset($param['query']['umur_end']);
		}
		if (!$paging) {
			unset($param['query']['offset']);
			unset($param['query']['limit']);
		}

		$request_value = $this->_ci_laporan->request('get', 'laporan_temp', $param);


		$value = json_decode($request_value->getBody()->getContents(), true);

		return $value;
	}

	function pdf()
	{
		$tgl_start = $this->input->get('tgl_start');
		$tgl_end = $this->input->get('tgl_end');
		$response_prop_clinic     = $this->_client_rs->request('GET', 'clinic_profile', [
			'query' => ['rs_key' => $this->_rs_key]
		]);
		$prop_clinic = json_decode($response_prop_clinic->getBody()->getContents(), true);
		$clinic_profile = $prop_clinic['data'];
		// id form header
		$id_form = $this->form_header;

		$form = $this->_client_ref->request('GET', 'form', [
			'query' => ['id' => $id_form]
		]);

		$form = json_decode($form->getbody()->getcontents(), true);
		$form = $form['data'];

		$title  = $form['nama_form']; // title pdf
		$kode   = $form['kode_form']; // kode form
		$name   = $form['pdf_file_name'] . $tgl_start . '_' . $tgl_end . '.pdf'; // nama form

		$value = $this->_getvalue();

		$columnDeclaration2 = [
			'no_rm' => 30, 'no_bpjs' => 35, 'nama_pasien' => 60, 'kelamin' => 35, 'penjamin' => 40, 'umur' => 50
		];

		$this->load->library("pdf");

		$pdf = new PDF('l', 'mm', 'A4');
		$pdf->AliasNbPages();
		$pdf->AddPage();
		// Header
		$pdf->SetFont('Arial', '', 16);
		$pdf->Cell(0, 2, $clinic_profile['clinic_name'], 0, 2, 'L');
		$pdf->Ln();

		$pdf->SetFont('Arial', '', 20);
		$pdf->Cell(0, 5, $title . ' ' . $kode, 0, 2, 'L');
		$pdf->Ln();

		$pdf->SetFont('Arial', 'B', 10);
		$pdf->Cell(16, 10, 'Periode : ' . date('Y-m-d', strtotime($tgl_start)) . ' - ' . date('Y-m-d', strtotime($tgl_end)), 0, 0, 'L');
		$pdf->Cell(260, 10, 'Report generated at ' . date('Y-m-d H:i:s', time()), 0, 0, 'R');

		$pdf->Ln();

		$pdf->SetFillColor(231, 231, 231);
		$pdf->SetDrawColor(128);
		$pdf->SetTextColor(0, 0, 0);

		$pdf->SetFont('Arial', '', 10);
		$pdf->Cell(10, 8, "No", 1, 0, 'C', true);
		$pdf->Cell(30, 8, "No RM", 1, 0, 'C', true);
		$pdf->Cell(40, 8, "No BPJS", 1, 0, 'C', true);
		$pdf->Cell(65, 8, "Nama", 1, 0, 'C', true);
		$pdf->Cell(30, 8, "Jenis Kelamin", 1, 0, 'C', true);
		$pdf->Cell(40, 8, "Penjamin", 1, 0, 'C', true);
		$pdf->Cell(60, 8, "Umur", 1, 0, 'C', true);
		$pdf->Ln();

		// End of HeaderFtr
		$pdf->Cell(275, 2, '', 1, 0, 'C');
		$pdf->Ln();
		$pdf->SetFont('Arial', '', 8);
		$i = 1;
		// trace($value['data']);
		foreach ($value['data'] as $k => $v) {
			$pdf->Cell(10, 8, $i++, 1, 0, 'C');
			$pdf->Cell(30, 8, $v['data_pasien']['no_rm'], 1, 0, 'C');
			$pdf->Cell(40, 8, $v['data_pasien']['no_bpjs'], 1, 0, 'C');
			$pdf->Cell(65, 8, $v['data_pasien']['nama_pasien'], 1, 0, 'L');
			$pdf->Cell(30, 8, $v['data_pasien']['kelamin'], 1, 0, 'C');
			$pdf->Cell(40, 8, $v['data_pasien']['penjamin'], 1, 0, 'C');
			$pdf->Cell(60, 8, $v['data_pasien']['umur'], 1, 0, 'C');
			$pdf->Ln();
		}

		$pdf->Output('I', $name);
	}

	function base64ToImage($base64_string, $output_file)
	{
		$file = fopen($output_file, "wb");

		$data = explode(',', $base64_string);

		fwrite($file, base64_decode($data[1]));
		fclose($file);

		return $output_file;
	}

	function excel()
	{
		// clicic profile
		$response_prop_clinic     = $this->_client_rs->request('GET', 'clinic_profile', [
			'query' => ['rs_key' => $this->_rs_key]
		]);
		$prop_clinic = json_decode($response_prop_clinic->getBody()->getContents(), true);
		$clinic_profile = $prop_clinic['data'];

		$tgl_start = $this->input->get('tgl_start');
		$tgl_end = $this->input->get('tgl_end');

		// id form header
		$id_form = $this->form_header;
		$form = $this->_client_ref->request('GET', 'form', [
			'query' => ['id' => $id_form]
		]);

		$form = json_decode($form->getbody()->getcontents(), true);
		$form = $form['data'];

		$title  = $form['nama_form']; // title pdf
		$kode   = $form['kode_form']; // kode form

		$value = $this->_getvalue(false);

		$writer = WriterEntityFactory::createXLSXWriter();
		$writer->setTempFolder('assets/temp/');
		$namaFile = $title . '-' . $kode . date('Y-m-d') . '.xlsx';
		$filePath = 'assets/temp/' . $namaFile;
		$writer->openToFile($filePath);

		$styleTitle = (new StyleBuilder())
			->setFontName('Arial')
			->setFontSize(16)
			->build();
		$styleSubTitle = (new StyleBuilder())
			->setFontName('Arial')
			->setFontSize(12)
			->build();

		$clinic[] = $clinic_profile['clinic_name'];
		$body = WriterEntityFactory::createRowFromArray($clinic, $styleTitle);
		$writer->addRow($body);

		$clinic_address[] = $clinic_profile['address'];
		$body = WriterEntityFactory::createRowFromArray($clinic_address, $styleSubTitle);
		$writer->addRow($body);

		$judul[] = $title;
		$body = WriterEntityFactory::createRowFromArray($judul, $styleTitle);
		$writer->addRow($body);
		/** Create a style with the StyleBuilder */
		$style = (new StyleBuilder())
			->setFontBold()
			->setFontSize(10)
			->setFontColor(Color::BLUE)
			// ->setShouldWrapText()
			->setBackgroundColor(Color::YELLOW)
			->build();

		/** Create a row with cells and apply the style to all cells */
		$dataHeader = array();
		$dataHeader[] = '#';
		$dataHeader[] = 'No RM';
		$dataHeader[] = 'No BPJS';
		$dataHeader[] = 'Nama Pasien';
		$dataHeader[] = 'Jenis Kelamin';
		$dataHeader[] = 'Penjamin';
		$dataHeader[] = 'Umur';
		$dataHeader[] = 'Alamat';
		$dataHeader[] = 'Status Pasien';
		$dataHeader[] = 'Kunjungan Ke-';
		$dataHeader[] = 'Nomor Registrasi';
		$dataHeader[] = 'Asal Pasien';
		$dataHeader[] = 'Nama DPJP';
		$dataHeader[] = 'Tanggal Check In';
		$dataHeader[] = 'Jenis Rawat';
		$dataHeader[] = 'Ruang Rawat';
		$dataHeader[] = 'ICD 10 Awal';

		$defaultStyle = (new StyleBuilder())
			->setFontName('Arial')
			->setFontSize(10)
			// ->setShouldWrapText(true)
			->build();

		$writer->setDefaultRowStyle($defaultStyle);

		$borderDefa = (new BorderBuilder())
			->setBorderBottom(Color::BLACK, Border::WIDTH_THIN, Border::STYLE_SOLID)
			->setBorderTop(Color::BLACK, Border::WIDTH_THIN, Border::STYLE_SOLID)
			->setBorderRight(Color::BLACK, Border::WIDTH_THIN, Border::STYLE_SOLID)
			->setBorderLeft(Color::BLACK, Border::WIDTH_THIN, Border::STYLE_SOLID)
			->build();

		$styleHeader = (new StyleBuilder())
			->setFontBold()
			->setBorder($borderDefa)
			->setBackgroundColor(Color::BLUE)
			->build();

		$row = WriterEntityFactory::createRowFromArray($dataHeader, $styleHeader);
		$writer->addRow($row);

		$styleData = (new StyleBuilder())
			->setBorder($borderDefa)
			->build();

		$i = 1;
		// trace($value);
		foreach ($value['data'] as $key => $val) {
			$row = array();
			$data_pasien = $val['data_pasien'];
			$data_registrasi = $val['data_registrasi'];
			$data_catatan = $val['data_catatan'];

			$row[] = $i++;
			$row[] = $data_pasien['no_rm'];
			$row[] = $data_pasien['no_bpjs'];
			$row[] = $data_pasien['nama_pasien'];
			$row[] = $data_pasien['kelamin'];
			$row[] = $data_pasien['penjamin'];
			$row[] = $data_pasien['umur'];
			$row[] = $data_pasien['alamat_1'] . ', ' . $data_pasien['alamat_2'];
			$row[] = $data_pasien['status'];
			$row[] = $data_pasien['kunjungan_ke'];
			$row[] = $data_registrasi['no_reg'];
			$row[] = $data_registrasi['asal_pasien'];
			$row[] = $data_registrasi['nama_dpjp'];
			$row[] = $data_registrasi['tgl_checkin'];
			$row[] = $data_registrasi['jenis_rawat'];
			$row[] = $data_registrasi['ruang_rawat'];

			foreach ($data_catatan['icd_10_awal'] as $k => $v) {
				$icd_10_awal = $k;
			}
			$row[] = $icd_10_awal;

			$body = WriterEntityFactory::createRowFromArray($row, $styleData);
			$writer->addRow($body);
		}


		/** Add the row to the writer */
		$writer->close();
		force_download($filePath, null);
	}
}
