<?php
defined('BASEPATH') or exit('No direct script access allowed');

use GuzzleHttp\Psr7;
use \GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Exception\ServerException;

class C_crud_temp extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();

        $token = "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJuYW1lIjoiTkFNQSAyIiwiaWRfdXNlciI6IjM3MzY4MSIsInJtX251bWJlciI6ImFkbWluIiwicnNfa2V5IjoiQTEyMyIsImlwX2FkZHJlc3MiOiIxMjcuMC4xLjEiLCJhY2Nlc3MiOiJ1c2VyIn0.ubW6fyc7ErYOW2T5qFbjXvLIVTLp05s3A0paQ6wfcmo";
        // guzzle client
        $this->_client_rs = new Client([
            'base_uri'  => $this->config->item('api_rs'),
            'headers'   => [
                'Content-Type' => 'application/json',
                'x-token' => $token
            ]
        ]);

        $this->_ci_laporan = new Client([
            'base_uri'  => $this->config->item('ci_laporan'),
            'headers'   => [
                'Content-Type' => 'application/json',
                'x-token' => $token
            ]
        ]);

        $this->_client_ref = new Client([
            'base_uri'  => $this->config->item('api_ref'),
            'headers'   => [
                'Content-Type' => 'application/json',
                'x-token' => $token
            ]
        ]);

        // Variable yang diperluka, tidak perlu diubah ubah
        $this->class = str_replace("c_", "", $this->router->fetch_class());
        $this->_rs_key   = '900614f7-7acd-11e8-a953-fa163e101f72';
        $this->_id_user  = '373680'; // id petugas input utk variable created_by
        $this->_no_rm    = '008000649'; // no rm pasien

    }

    public function index()
    {
        $data['class_name'] = $this->class;
        $data['contents'] = 'contents/crud/' . $this->class . '/index';
        $this->load->view('master', $data);
    }

    public function list()
    {
        // trace($this->class);
        $crud_temp = $this->_client_rs->request('GET', 'crud_temp');
        $crud_temp = json_decode($crud_temp->getBody()->getContents(), true)['data'];
        $data['crud_temp'] = $crud_temp;
        // trace($crud_temp);
        $this->load->view('contents/crud/' . $this->class . '/list', $data);
    }

    public function add()
    {
        $departements = $this->_client_rs->request('GET', 'departements');
        $departements = json_decode($departements->getBody()->getContents(), true)['data'];

        $penjamin = $this->_client_rs->request('GET', 'ref_rs/penjamin');
        $penjamin = json_decode($penjamin->getBody()->getContents(), true)['data'];

        $bt_order_dept = $this->_client_rs->request('GET', 'ref_rs/bt_order_dept');
        $bt_order_dept = json_decode($bt_order_dept->getBody()->getContents(), true)['data'];

        $data['departements'] = $departements;
        $data['bt_order_dept'] = $bt_order_dept;
        $data['penjamin'] = $penjamin;
        $this->load->view('contents/crud/' . $this->class . '/add', $data);
    }

    public function add_process()
    {
        $params = $this->input->post();
        $this->_config['body'] = json_encode($params);
        // trace(json_encode($params));
        $resp = $this->_client_rs->request('POST', 'crud_temp', $this->_config);
        $resp = json_decode($resp->getBody()->getContents(), true);
        echo json_encode($resp);
        // trace($params);
    }

    public function edit()
    {
        $params = [
            'query' => [
                'id' => $this->input->post('id')
            ]
        ];
        
        $detail = $this->_client_rs->request('GET', 'crud_temp', $params);
        $detail = json_decode($detail->getBody()->getContents(), true)['data'][0];

        $departements = $this->_client_rs->request('GET', 'departements');
        $departements = json_decode($departements->getBody()->getContents(), true)['data'];

        $penjamin = $this->_client_rs->request('GET', 'ref_rs/penjamin');
        $penjamin = json_decode($penjamin->getBody()->getContents(), true)['data'];

        $bt_order_dept = $this->_client_rs->request('GET', 'ref_rs/bt_order_dept');
        $bt_order_dept = json_decode($bt_order_dept->getBody()->getContents(), true)['data'];

        $data['departements'] = $departements;
        $data['bt_order_dept'] = $bt_order_dept;
        $data['penjamin'] = $penjamin;

        $data['detail'] = $detail;
        $this->load->view('contents/crud/' . $this->class . '/edit', $data);
    }

    public function edit_process()
    {
        $params = $this->input->post();
        // trace(json_encode($params));
        $this->_config['body'] = json_encode($params);
        $resp = $this->_client_rs->request('PUT', 'crud_temp', $this->_config);
        $resp = json_decode($resp->getBody()->getContents(), true);
        echo json_encode($resp);
    }

    public function delete()
    {
        $params = [
            'id' => $this->input->post('id'),
            'is_del'    => 1,
        ];
        $this->_config['body'] = json_encode($params);
        $resp = $this->_client_rs->request('DELETE', 'crud_temp', $this->_config);
        $resp = json_decode($resp->getBody()->getContents(), true);
        echo json_encode($resp);
    }
}
