<?php
defined('BASEPATH') or exit('No direct script access allowed');

use GuzzleHttp\Psr7;
use \GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Exception\ServerException;

class C_ref_users_blood_type extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();

        $token = "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJuYW1lIjoiTkFNQSAyIiwiaWRfdXNlciI6IjM3MzY4MSIsInJtX251bWJlciI6ImFkbWluIiwicnNfa2V5IjoiQTEyMyIsImlwX2FkZHJlc3MiOiIxMjcuMC4xLjEiLCJhY2Nlc3MiOiJ1c2VyIn0.ubW6fyc7ErYOW2T5qFbjXvLIVTLp05s3A0paQ6wfcmo";
        // guzzle client
        $this->_client_rs = new Client([
            'base_uri'  => $this->config->item('api_rs'),
            'headers'   => [
                'Content-Type' => 'application/json',
                'x-token' => $token
            ]
        ]);

        $this->_ci_laporan = new Client([
            'base_uri'  => $this->config->item('ci_laporan'),
            'headers'   => [
                'Content-Type' => 'application/json',
                'x-token' => $token
            ]
        ]);

        $this->_client_ref = new Client([
            'base_uri'  => $this->config->item('api_ref'),
            'headers'   => [
                'Content-Type' => 'application/json',
                'x-token' => $token
            ]
        ]);

        // Variable yang diperluka, tidak perlu diubah ubah
        $this->class = str_replace("c_", "", $this->router->fetch_class());
        $this->_rs_key   = '900614f7-7acd-11e8-a953-fa163e101f72';
        $this->_id_user  = '373680'; // id petugas input utk variable created_by
        $this->_no_rm    = '008000649'; // no rm pasien

    }

    public function index()
    //$route['ref_users_blood_type'] = 'ref/ref_users_blood_type/c_ref_users_blood_type';
    {
        $data['class_name'] = $this->class;
        $data['contents'] = 'contents/ref/' . $this->class . '/index';
        $this->load->view('master', $data);
    }

    public function list()
    //$route['ref_users_blood_type/(:any)'] = 'ref/ref_users_blood_type/c_ref_users_blood_type/$1';
    {
        // trace($this->class);
        $blood_type = $this->_client_ref->request('GET', 'ref_blood/blood');
        $blood_type = json_decode($blood_type->getBody()->getContents(), true)['data'];
        $data['blood'] = $blood_type;
        // trace($crud_temp);
        $this->load->view('contents/ref/' . $this->class . '/list', $data);
    }
}
