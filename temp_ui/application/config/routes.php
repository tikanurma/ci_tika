<?php
defined('BASEPATH') or exit('No direct script access allowed');

$route['default_controller'] = 'welcome';
$route['404_override'] = '';
$route['translate_uri_dashes'] = TRUE;


/// ROUTES START FOR NOTES_TEMP
$route['notes_temp_reg3'] = 'notes/notes_temp_reg3/c_notes_temp_reg3';
$route['notes_temp_reg3/(:any)'] = 'notes/notes_temp_reg3/c_notes_temp_reg3/$1';
$route['notes_temp_reg3/(:any)/(:any)']               = 'notes/notes_temp_reg3/c_notes_temp_reg3/$1/$2';
$route['notes_temp_reg3/(:any)/(:num)/(:any)']        = 'notes/notes_temp_reg3/c_notes_temp_reg3/$1/$2/$3';
$route['notes_temp_reg3/(:any)/(:any)/(:any)/(:any)'] = 'notes/notes_temp_reg3/c_notes_temp_reg3/$1/$2/$3/$4';

// ROUTES START FOR LAPORAN_TEMP
$route['laporan_temp'] = 'laporan/laporan_temp/c_laporan_temp';
$route['laporan_temp/(:any)'] = 'laporan/laporan_temp/c_laporan_temp/$1';

/// ROUTES START FOR CRUD_TEMP
$route['crud_temp'] = 'crud/crud_temp/c_crud_temp';
$route['crud_temp/(:any)'] = 'crud/crud_temp/c_crud_temp/$1';
$route['ref_suku_bangsa'] = 'ref/ref_suku_bangsa/c_ref_suku_bangsa';
$route['ref_suku_bangsa/(:any)'] = 'ref/ref_suku_bangsa/c_ref_suku_bangsa/$1';
$route['ref_users_blood_type'] = 'ref/ref_users_blood_type/c_ref_users_blood_type';
$route['ref_users_blood_type/(:any)'] = 'ref/ref_users_blood_type/c_ref_users_blood_type/$1';
$route['ref_users_occupation'] = 'ref/ref_users_occupation/c_ref_users_occupation';
$route['ref_users_occupation/(:any)'] = 'ref/ref_users_occupation/c_ref_users_occupation/$1';
$route['ref_users_pendidikan'] = 'ref/ref_users_pendidikan/c_ref_users_pendidikan';
$route['ref_users_pendidikan/(:any)'] = 'ref/ref_users_pendidikan/c_ref_users_pendidikan/$1';
$route['ref_users_emergency_contact_relationship_to_patient'] = 'ref/ref_users_emergency_contact_relationship_to_patient/c_ref_users_emergency_contact_relationship_to_patient';
$route['ref_users_emergency_contact_relationship_to_patient/(:any)'] = 'ref/ref_users_emergency_contact_relationship_to_patient/c_ref_users_emergency_contact_relationship_to_patient/$1';
$route['ref_rekam_medis_category'] = 'ref/ref_rekam_medis_category/c_ref_rekam_medis_category';
$route['ref_rekam_medis_category/(:any)'] = 'ref/ref_rekam_medis_category/c_ref_rekam_medis_category/$1';