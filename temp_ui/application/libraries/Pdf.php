<?php
include_once FCPATH . 'assets/fpdf/fpdf.php';
class Pdf extends FPDF {

    function __construct($orientation='L', $unit='mm', $size='A4'){
        parent::__construct($orientation,$unit,$size);
    }

    function Footer() {        
        $this->SetFont('Arial','',12);
        $this->SetY(-15);

        $this->Cell(0,10,'Halaman : '.$this->PageNo().'/{nb}',0,0,'R');
    }
}
?>