<a href="#" class="btn btn-primary btn-sm tambah pull-right">Tambah</a>
<br><br>
<table class="table nowrap datatables">
  <thead>
    <tr>
      <th>#</th>
      <th></th>
      <th>Nama Penjamin</th>
      <th>Nama Depo</th>
      <th>Jenis Pasien</th>
      <th>Nama Tarif</th>
      <th>Harga Tarif</th>
    </tr>
  </thead>
  <tbody>
    <?php $i=1; foreach ($crud_temp as $d) : ?>
      <tr>
        <td><?= $i++; ?></td>
        <td>
          <button class="btn btn-sm btn-primary edit" data-id="<?= $d['id']; ?>">Edit</button>
          <button class="btn btn-sm btn-danger delete" data-id="<?= $d['id']; ?>">Delete</button>
        </td>
        <td><?= $d['nama_penjamin']; ?></td>
        <td><?= $d['nama_dept']; ?></td>
        <td><?= $d['jenis_pasien']; ?></td>
        <td><?= $d['tarif_dept'][0]['nama_tarif']; ?></td>
        <td><?= idr_format($d['tarif_dept'][0]['harga_tarif']); ?></td>

      </tr>
    <?php endforeach; ?>
  </tbody>
</table>
