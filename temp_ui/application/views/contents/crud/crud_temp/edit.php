<div class="col-lg-6">
	<div class="panel panel-primary">
		<div class="panel-heading">Form Edit Auto Tarif</div>
		<div class="panel-body">
			<form id="form-edit" class="form-horizontal">
                <input type="hidden" name="id" id="id" value="<?=$detail['id'];?>">
				<div class="form-group">
					<label class="control-label col-lg-3">Nama Depo :</label>
					<div class="col-lg-9">
						<select name="id_dept" id="id_dept" class="form-control">
							<?php foreach ($departements as $d) : ?>
								<option value="<?= $d['departement_id']; ?>" <?= $detail['id_dept'] == $d['departement_id'] ? 'selected' :'';?>><?= $d['departement_name']; ?></option>
							<?php endforeach; ?>
						</select>
					</div>
				</div>

				<div class="form-group">
					<label class="control-label col-lg-3">Penjamin :</label>
					<div class="col-lg-9">
						<select name="payment_prefix" id="payment_prefix" class="form-control">
							<?php foreach ($penjamin as $d) : ?>
								<option value="<?= $d['prefix']; ?>" <?= $detail['prefix_penjamin'] == $d['prefix'] ? 'selected' :'';?>><?= $d['nama']; ?></option>
							<?php endforeach; ?>
						</select>
					</div>
				</div>

				<div class="form-group">
					<label class="control-label col-lg-3">Bt Order Dept :</label>
					<div class="col-lg-9">
						<select name="id_bt_order_dept" id="id_bt_order_dept" class="form-control">
							<?php foreach ($bt_order_dept as $d) : ?>
								<option value="<?= $d['id_bt_order']; ?>" <?= $d['id_bt_order'] == $detail['id_tarif_dept'] ? 'selected' :'';?>><?= $d['nama_master_dept_job']; ?></option>
							<?php endforeach; ?>
						</select>
					</div>
				</div>

				<div class="form-group">
					<label class="control-label col-lg-3">Jenis Pasien :</label>
					<input type="hidden" name="jenis_pasien" id="jenis_pasien" value="<?=$detail['jenis_pasien'];?>">
					<div class="col-lg-9 jenis-pasien">
						<button type="button" class="btn btn-default btn-sm <?=$detail['jenis_pasien'] == "LAMA" ? 'btn-primary' : '';?>" data-id="LAMA">Lama</button>
						<button type="button" class="btn btn-sm btn-default <?=$detail['jenis_pasien'] == "BARU" ? 'btn-primary' : '';?>" data-id="BARU">Baru</button>
						<button type="button" class="btn btn-sm btn-default <?=$detail['jenis_pasien'] == "ALL" ? 'btn-primary' : '';?>" data-id="ALL">All</button>
					</div>
				</div>

				<div class="form-group">
					<div class="col-lg-offset-8 col-lg-10">
						<a href="#" class="btn btn-default back"> Batal</a>
						<button type="submit" class="btn btn-primary">Simpan</button>
					</div>
				</div>
			</form>
		</div>
	</div>
</div>

<script>
	$('#id_dept').select2();
	$('#payment_prefix').select2();
	$('#id_bt_order_dept').select2();
	$('.jenis-pasien').on('click', '.btn', function() {
        $('.jenis-pasien .btn-primary').removeClass('btn-primary')
        $(this).addClass('btn-primary')
        jenis = $(this).data('id')
        $('#jenis_pasien').val(jenis);
      })
	$('#form-edit').submit(function(e) {
		e.preventDefault();
		data = $(this).serialize();
		$.post('<?= base_url() ?>' + class_name + '/edit_process', data, function(data) {
			var data = JSON.parse(data);
			if (data.status == '200') {
				alert(data.message);
				location.reload();
			} else {
				alert('Gagal mengirim data.');
			}
		});
	});

</script>
