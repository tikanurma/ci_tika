<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/chart.js@2.9.3/dist/Chart.min.js"></script>
<link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/css/select2.min.css" rel="stylesheet" />
<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/js/select2.min.js"></script>

<div class="col-md-12" style="padding: 0px;">
  <br />
  <div class="row">
    <div class="col-md-12">
      <div class="col-md-3">
        <div style="margin-bottom: 30px"><b></b></div>
        <div class="col-md-12 is-pilih-filter">
          <button type="button" class="btn btn-default btn-primary btn-sm" data-id="harian">Harian</button>
          <button type="button" class="btn btn-sm btn-default" data-id="range">Range Tanggal</button>
          <button type="button" class="btn btn-sm btn-default" data-id="bulanan">Bulanan</button>
        </div>
      </div>
      <div class="col-md-3 filter-harian" style="display: block">
        <div style="margin-bottom: 10px"><b>Harian</b></div>
        <div class="row">
          <div class="col-md-12">
            <input type="text" name="tanggal" id="tanggal" class="form-control" value="<?= date('Y-m-d') ?>" autocomplete="off">
          </div>
        </div>
      </div>
      <div class="col-md-3 filter-harian" style="display: block"></div>
      <div class="col-md-3 filter-range" style="display: none">
        <div style="margin-bottom: 10px"><b>Range</b></div>
        <div class="row">
          <div class="col-md-12">
            <input type="text" class="form-control" name="daterange" id="daterange" value="<?= date('Y-m-01', strtotime('-3 months')); ?> - <?= date('Y-m-t'); ?>" />
          </div>
        </div>
      </div>
      <div class="col-md-3 filter-range" style="display: none"></div>
      <div class="col-md-3 filter-bulanan" style="display: none">
        <div style="margin-bottom: 10px"><b>Bulan</b></div>
        <div class="row">
          <div class="col-md-12">
            <div class="">
              <?php
              $arrDay = array(
                1   =>  'Januari',
                2   =>  'Februari',
                3   =>   'Maret',
                4   =>  'April',
                5   =>  'Mei',
                6   =>  'Juni',
                7   =>  'Juli',
                8   =>  'Agustus',
                9   =>  'September',
                10  =>  'Oktober',
                11  =>  'November',
                12  =>  'Desember'
              );
              ?>
              <?php for ($a = 1; $a <= 12; $a++) : ?>
                <button style="margin:2px" class="btn btn-default is-bulan btn-sm <?php echo ((date("n")) == $a) ? "btn-primary" : ""; ?>" name="datatype" data-id="<?php echo (strlen($a) == 1) ? '0' . $a : $a; ?>"><?php echo $arrDay[$a]; ?></button>
              <?php endfor; ?>
            </div>
          </div>
        </div>
      </div>
      <div class="col-md-3 filter-bulanan" style="display: none">
        <div style="margin-bottom: 10px"><b>Tahun</b></div>
        <?php for ($i = 2017; $i <= date('Y'); $i++) : ?>
          <button style="margin:2px" data-id="<?php echo $i; ?>" class="btn btn-sm btn-default <?php echo (date("Y") == $i) ? "btn-primary" : ""; ?> is-tahun"><?php echo $i; ?></button>
        <?php endfor; ?>
      </div>
      <div class="col-md-3">
        <div style="margin-bottom: 30px"><b></b></div>
        <div>
          <a target='_blank' href="#" class="cetak_pdf pull-right">
            <button style="margin:2px" class="btn btn-sm btn-primary" type="button" name="button">
              <i class="fa fa-print">&nbsp;</i>PDF
            </button>
          </a>
          <a target='_blank' href="#" class="cetak_xls pull-right">
            <button style="margin:2px" class="btn btn-sm btn-success" type="button" name="button">
              <i class="fa fa-download">&nbsp;</i>Excel
            </button>
          </a>
          <button type="button" data-stat="0" class="btn btn-sm btn-warning pull-right btn-filter" style="margin:2px; margin-right: 30px">Buka Filter</button>
        </div>
      </div>
    </div>
  </div>

  <div class="col-md-12 filter" style="display:none">
    <div class="row" style="margin-top:12px;margin-bottom:12px">
      <div class="col-md-4 is-periode">
        <div style="margin-bottom:10px"><b>Jenis Periode</b></div>
        <button type="button" class="btn btn-default btn-primary btn-sm" data-id="1" disabled>Waktu Masuk</button>
        <button type="button" class="btn btn-sm btn-default" data-id="2" disabled>Waktu Keluar</button>
      </div>
      <div class="col-md-4 is-shift">
        <div style="margin-bottom:10px"><b>Shift</b></div>
        <button type="button" class="btn btn-default btn-primary btn-sm" data-id="">Semua</button>
        <button type="button" class="btn btn-sm btn-default" data-id="1">Shift 1</button>
        <button type="button" class="btn btn-sm btn-default" data-id="2">Shift 2</button>
        <button type="button" class="btn btn-sm btn-default" data-id="3">Shift 3</button>
      </div>
      <div class="col-md-4 is-pasien">
        <div style="margin-bottom:10px"><b>Pasien</b></div>
        <button type="button" class="btn btn-default btn-primary btn-sm" data-id="">Semua</button>
        <button type="button" class="btn btn-sm btn-default" data-id="1">Pasien Lama</button>
        <button type="button" class="btn btn-sm btn-default" data-id="2">Pasien Baru</button>
      </div>
    </div>
    <div class="row" style="margin-top:12px;margin-bottom:12px">
      <div class="col-md-4 is-gender">
        <div style="margin-bottom:10px"><b>Jenis Kelamin</b></div>
        <button type="button" class="btn btn-default btn-primary btn-sm" data-id="">Semua</button>
        <button type="button" class="btn btn-default btn-sm" data-id="1">Laki-laki</button>
        <button type="button" class="btn btn-sm btn-default" data-id="2">Perempuan</button>
      </div>
      <div class="col-md-8 is-umur">
        <div style="margin-bottom:10px"><b>Umur</b></div>
        <button type="button" style="margin:2px" class="btn btn-default btn-primary btn-sm" data-start="" data-end="">Semua</button>
        <button type="button" style="margin:2px" class="btn btn-default btn-sm" data-start="0 0 0" data-end="0 0 6">0 Hari - 6 Hari</button>
        <button type="button" style="margin:2px" class="btn btn-default btn-sm" data-start="0 0 7" data-end="0 0 28">7 Hari - 28 Hari</button>
        <button type="button" style="margin:2px" class="btn btn-default btn-sm" data-start="0 0 29" data-end="1 0 0">28 Hari - 1 Thn</button>
        <button type="button" style="margin:2px" class="btn btn-default btn-sm" data-start="1 0 1" data-end="4 11 29">1 Thn - 4 Thn 11 Bln</button>
        <button type="button" style="margin:2px" class="btn btn-default btn-sm" data-start="4 11 30" data-end="14 11 29">5 Thn - 14 Thn 11 Bln</button>
        <button type="button" style="margin:2px" class="btn btn-default btn-sm" data-start="14 11 30" data-end="24 11 29">15 Thn - 24 Thn 11 Bln</button>
        <button type="button" style="margin:2px" class="btn btn-default btn-sm" data-start="24 11 30" data-end="44 11 29">25 Thn - 44 Thn 11 Bln</button>
        <button type="button" style="margin:2px" class="btn btn-default btn-sm" data-start="44 11 30" data-end="64 11 29">45 Thn - 64 Thn 11 Bln</button>
        <button type="button" style="margin:2px" class="btn btn-default btn-sm" data-start="64 11 30" data-end="99 0 0">>65 Thn</button>
      </div>
    </div>
    <div class="row" style="margin-top:12px;margin-bottom:12px">
      <div class="col-md-4 is-ref">
        <div style="margin-bottom:10px"><b>Penjamin</b></div>
        <div class="is-ref-button">
          <button type="button" class="btn btn-default btn-primary btn-sm" data-id="" style="margin:2px">Semua</button>
          <?php
          foreach ($filter['data_penjamin'] as $key => $val) {
          ?>

            <button style="margin:2px" type="button" class="btn btn-default btn-sm" data-id="<?= $key ?>"><?= $val['nama'] ?> <span class="badge badge-light"><?= $val['total']; ?></span></button>
          <?php } ?>
        </div>
      </div>
      <div class="col-md-4 is-dokter">
        <div style="margin-bottom:10px"><b>Dokter</b></div>
        <div class="is-dokter-button">
          <button type="button" class="btn btn-default btn-primary btn-sm" data-id="" style="margin:2px">Semua</button>
          <?php
          foreach ($filter['data_dokter'] as $key => $val) {
          ?>

            <button style="margin:2px" type="button" class="btn btn-default btn-sm" data-id="<?= $key ?>"><?= $val['nama'] ?></button>
          <?php } ?>
        </div>
      </div>
      <div class="col-md-4">
        <div class="row">
          <div class="col-md-6">
            <div style="margin-bottom:10px"><b>Kecamatan</b></div>
            <select class="form-control is-kecamatan" data-live-search="true">
              <option value="">Semua</option>
              <?php
              foreach ($filter['data_kecamatan'] as $key => $val) {
              ?>

                <option value="<?= $key ?>"><?= $val['nama'] ?></option>
              <?php } ?>
            </select>
          </div>
          <div class="col-md-6">
            <div style="margin-bottom:10px"><b>Kelurahan</b></div>
            <select class="form-control is-kelurahan" data-live-search="true">
              <option value="">Semua</option>
              <?php
              foreach ($filter['data_kelurahan'] as $key => $val) {
              ?>

                <option value="<?= $key ?>"><?= $val['nama'] ?></option>
              <?php } ?>
            </select>
          </div>
        </div>

      </div>
    </div>
    <div class="row" style="margin-top:12px;margin-bottom:12px">
      <div class="col-md-4 is-rawat">
        <div style="margin-bottom:10px"><b>Ruang Rawat</b></div>
        <div class="is-rawat-button">
          <button type="button" class="btn btn-default btn-primary btn-sm" data-id="" style="margin:2px">Semua</button>
          <?php
          foreach ($filter['data_ruang_rawat'] as $key => $val) {
          ?>

            <button style="margin:2px" type="button" class="btn btn-default btn-sm" data-id="<?= $key ?>"><?= $val['nama'] ?> <span class="badge badge-light"><?= $val['total']; ?></span></button>
          <?php } ?>
        </div>
      </div>
    </div>
  </div>

  <div class="col-md-12 bold text-center">
    <strong>
      <h1><?= $title ?></h1>
    </strong>
  </div>

  <div class="col-md-12">
    <div class="table-responsive">
      <table class="table table-bordered" id="table">
        <thead class="thead bg-secondary" style="background-color: #e7e7e7;">
          <tr>
            <th class="text-center v-center">#</th>
            <th class="text-center v-center">RM</th>
            <th class="text-center v-center">BPJS</th>
            <th class="text-center v-center">Nama Pasien</th>
            <th class="text-center v-center">Jenis Kelamin</th>
            <th class="text-center v-center">Penjamin</th>
            <th class="text-center v-center">Umur</th>
            <th class="text-center v-center">Alamat</th>
            <th class="text-center v-center">Status Pasien</th>
            <th class="text-center v-center">Kunjungan Ke</th>
            <th class="text-center v-center">Nomor Registrasi</th>
            <th class="text-center v-center">Asal Pasien</th>
            <th class="text-center v-center">Nama DPJP</th>
            <th class="text-center v-center">Tanggal Checkin</th>
            <th class="text-center v-center">Jenis Rawat</th>
            <th class="text-center v-center">Ruang Rawat</th>
            <th class="text-center v-center">ICD 10 Awal</th>
          </tr>
        </thead>
        <tbody>
        </tbody>
      </table>
    </div>
  </div>

  <script type="text/javascript">
    $(document).ready(function() {
      $('.is-kecamatan').select2({
        dropdownAutoWidth: true
      });
      $('.is-kelurahan').select2({
        dropdownAutoWidth: true
      });
      $('.btn-filter').on('click', function() {
        $('.filter').toggle();

        if ($('.filter').is(":visible")) {
          $(this).removeClass('btn-warning');
          $(this).addClass('btn-danger');

          $(this).html("Tutup Filter");
        } else {
          $(this).removeClass('btn-danger');
          $(this).addClass('btn-warning');
          $(this).html("Buka Filter");
        }
      })

      let target = "<?= $url ?>";
      let month = $('.is-bulan.btn-primary').data('id')
      let year = $('.is-tahun.btn-primary').data('id')
      let jenis_periode = $('.is-periode .btn-primary').data('id');
      let shift = $('.is-shift .btn-primary').data('id');
      let is_pasien_baru = $('.is-pasien .btn-primary').data('id');
      let gender = $('.is-gender .btn-primary').data('id');
      let id_ref_payment = $('.is-ref .btn-primary').data('id');
      let id_dpjp = $('.is-dokter .btn-primary').data('id');
      let id_kec = $('.is-kecamatan').val();
      let id_kel = $('.is-kelurahan').val();
      let umur_start = $('.is-umur .btn-primary').data('start');
      let umur_end = $('.is-umur .btn-primary').data('end');
      let id_dept_ruang_rawat = $('.is-rawat .btn-primary').data('id');
      let date_start = "<?= date('d'); ?>";
      let month_start = $('.is-bulan.btn-primary').data('id')
      let year_start = $('.is-tahun.btn-primary').data('id')
      let date_end = "<?= date('d'); ?>";
      let month_end = $('.is-bulan.btn-primary').data('id')
      let year_end = $('.is-tahun.btn-primary').data('id')

      const getParam = () => {
        const query = {
          tgl_start: `${year_start}-${month_start}-${date_start}`,
          tgl_end: `${year_end}-${month_end}-${date_end}`,
          jenis_periode: `${jenis_periode}`,
          shift: `${shift}`,
          is_pasien_baru: `${is_pasien_baru}`,
          gender: `${gender}`,
          umur_start: `${umur_start}`,
          umur_end: `${umur_end}`,
          id_ref_payment: `${id_ref_payment}`,
          id_dpjp: `${id_dpjp}`,
          id_kec: `${id_kec}`,
          id_kel: `${id_kel}`,
          id_dept_ruang_rawat: `${id_dept_ruang_rawat}`
        }
        return $.param(query)
      }

      const query = (d) => {
        d.tgl_start = `${year_start}-${month_start}-${date_start}`;
        d.tgl_end = `${year_end}-${month_end}-${date_end}`;
        d.jenis_periode = `${jenis_periode}`;
        d.shift = `${shift}`;
        d.is_pasien_baru = `${is_pasien_baru}`;
        d.gender = `${gender}`;
        d.umur_start = `${umur_start}`;
        d.umur_end = `${umur_end}`;
        d.id_ref_payment = `${id_ref_payment}`;
        d.id_dpjp = `${id_dpjp}`;
        d.id_kec = `${id_kec}`;
        d.id_kel = `${id_kel}`;
        d.id_dept_ruang_rawat = `${id_dept_ruang_rawat}`;
      }

      const loopButton = (key, nama, primary = false) => {
        var primaryClass = "";
        if (primary) {
          primaryClass = " btn-primary";
        }
        return '<button style="margin:2px" type="button" class="btn btn-default btn-sm' + primaryClass + '" data-id="' + key + '">' + nama + '</button>';
      }

      const loopOption = (key, nama) => {
        return '<option value="' + key + '">' + nama + '</option>';
      }

      const refreshFilter = () => {
        const params = getParam()
        $.getJSON(`${target}/filter?${params}`, function(response) {
          var refHTML = loopButton("", "Semua", true);
          var dokterHTML = refHTML;
          var rawatHTML = refHTML;
          var kecamatanHTML = loopOption("", "Semua");
          var kelurahanHTML = kecamatanHTML;

          $.each(response.data_penjamin, function(i, val) {
            refHTML += loopButton(i, val.nama + ' <span class="badge badge-light">' + val.total + '</span>')
          })

          $.each(response.data_dokter, function(i, val) {
            dokterHTML += loopButton(i, val.nama + ' <span class="badge badge-light">' + val.total + '</span>');
          });
          $.each(response.data_ruang_rawat, function(i, val) {
            rawatHTML += loopButton(i, val.nama + ' <span class="badge badge-light">' + val.total + '</span>');
          });
          $.each(response.data_kecamatan, function(i, val) {
            kecamatanHTML += loopOption(i, val.nama + ' [L(' + val.pria + ') P(' + val.wanita + ')]');
          });
          $.each(response.data_kelurahan, function(i, val) {
            kelurahanHTML += loopOption(i, val.nama + ' [L(' + val.pria + ') P(' + val.wanita + ')]');
          });

          $('.is-ref-button').html(refHTML);
          $('.is-dokter-button').html(dokterHTML);
          $('.is-rawat-button').html(rawatHTML);
          $('.is-kecamatan').html(kecamatanHTML);
          $('.is-kelurahan').html(kelurahanHTML);
        });
      }

      const initiateDatatables = () => {
        $("#table").DataTable({
          autoWidth: false,
          scrollX: true,
          ordering: false,
          "processing": true,
          "serverSide": true,
          "ajax": {
            "url": `${target}/json`,
            "type": 'POST',
            "data": query
          }
        });
      }

      initiateDatatables();
      refreshFilter();

      const refresh = () => {
        $('#table').DataTable().ajax.reload();
      }

      $('#sub_menu_referensi > a').on('click', function(e) {
        e.preventDefault()
        target = $(this).attr('href')
        $('.nav.menu.black-selected').removeClass('black-selected')
        $(this).addClass('black-selected')
        refresh()
      })

      $('.is-periode').on('click', '.btn', function() {
        $('.is-periode .btn-primary').removeClass('btn-primary')
        $(this).addClass('btn-primary')
        jenis_periode = $(this).data('id')
        refresh()
      })
      $('.is-shift').on('click', '.btn', function() {
        $('.is-shift .btn-primary').removeClass('btn-primary')
        $(this).addClass('btn-primary')
        shift = $(this).data('id')
        refresh()
      })
      $('.is-rawat').on('click', '.btn', function() {
        $('.is-rawat .btn-primary').removeClass('btn-primary')
        $(this).addClass('btn-primary')
        id_dept_ruang_rawat = $(this).data('id')
        refresh()
      })
      $('.is-pasien').on('click', '.btn', function() {
        $('.is-pasien .btn-primary').removeClass('btn-primary')
        $(this).addClass('btn-primary')
        is_pasien_baru = $(this).data('id')
        refresh()
      })
      $('.is-gender').on('click', '.btn', function() {
        $('.is-gender .btn-primary').removeClass('btn-primary')
        $(this).addClass('btn-primary')
        gender = $(this).data('id')
        refresh()
      })
      $('.is-umur').on('click', '.btn', function() {
        $('.is-umur .btn-primary').removeClass('btn-primary')
        $(this).addClass('btn-primary')
        umur_start = $(this).data('start')
        umur_end = $(this).data('end')
        refresh()
      })
      $('.is-ref').on('click', '.btn', function() {
        $('.is-ref .btn-primary').removeClass('btn-primary')
        $(this).addClass('btn-primary')
        id_ref_payment = $(this).data('id')
        refresh()
      })
      $('.is-dokter').on('click', '.btn', function() {
        $('.is-dokter .btn-primary').removeClass('btn-primary')
        $(this).addClass('btn-primary')
        id_dpjp = $(this).data('id')
        refresh()
      })

      $('.is-kecamatan').on('change', function() {
        id_kec = $('.is-kecamatan').val();
        refresh();
      })
      $('.is-kelurahan').on('change', function() {
        id_kel = $('.is-kelurahan').val();
        refresh();
      })

      $('.is-bulan').on('click', function() {
        $('.is-bulan.btn-primary').removeClass('btn-primary')
        $(this).addClass('btn-primary')
        month_start = $(this).data('id')
        month_end = $(this).data('id')
        date_start = '01'
        date_end = '30'
        refresh()
        refreshFilter();
      })

      $('.is-tahun').on('click', function() {
        $('.is-tahun.btn-primary').removeClass('btn-primary')
        $(this).addClass('btn-primary')
        year_start = $(this).data('id')
        year_end = $(this).data('id')
        date_start = '01'
        date_end = '30'
        refresh()
        refreshFilter();
      })

      $('.cetak_pdf').click(function() {
        const params = getParam()
        $(this).attr('href', `${target}/pdf?${params}`)
      })
      $('.cetak_xls').click(function() {
        const params = getParam()
        $(this).attr('href', `${target}/excel?${params}`)
      })
      $('#daterange').daterangepicker({
        opens: 'left',
        locale: {
          format: 'YYYY-MM-DD' // --------Here
        },
      });

      $('#daterange').on('apply.daterangepicker', function(ev, picker) {
        console.log(picker.startDate.format('DD'));
        console.log(picker.endDate.format('YYYY-MM-DD'));
        date_start = picker.startDate.format('DD');
        date_end = picker.endDate.format('DD');
        month_start = picker.startDate.format('MM');
        month_end = picker.endDate.format('MM');
        year_start = picker.startDate.format('YYYY');
        year_end = picker.endDate.format('YYYY');
        refresh();
        refreshFilter();

      });

      $('#tanggal').datetimepicker({
        format: "YYYY-MM-DD",
        showTodayButton: true,
        timeZone: '',
        dayViewHeaderFormat: 'MMMM YYYY',
        stepping: 5,
        locale: moment.locale(),
        collapse: true,
        icons: {
          time: 'fa fa-clock-o',
          date: 'fa fa-calendar',
          up: 'fa fa-chevron-up',
          down: 'fa fa-chevron-down',
          previous: 'fa fa-chevron-left',
          next: 'fa fa-chevron-right',
          today: 'fa fa-crosshairs',
          clear: 'fa fa-trash-o',
          close: 'fa fa-times'
        },
        sideBySide: true,
        calendarWeeks: false,
        viewMode: 'days',
        viewDate: false,
        toolbarPlacement: 'bottom',
        widgetPositioning: {
          horizontal: 'left',
          vertical: 'bottom'
        }
      });

      $('#tanggal').on('dp.change', function(e) {
        var tanggal = e.date.format(e.date._f);
        tanggal = tanggal.split("-");
        year_start = tanggal[0];
        year_end = tanggal[0];
        month_start = tanggal[1];
        month_end = tanggal[1];
        date_start = tanggal[2];
        date_end = tanggal[2];
        refresh();
        refreshFilter();
      })


      // Filter
      $('.is-pilih-filter').on('click', '.btn', function() {
        $('.is-pilih-filter .btn-primary').removeClass('btn-primary')
        $(this).addClass('btn-primary')
        pilih_filter = pilihFilter($(this).data('id'))
      });

      function pilihFilter(filter) {
        if (filter == 'range') {
          $('.filter-harian').hide();
          $('.filter-bulanan').hide();
          $('.filter-range').show();
          date_start = '01'
          date_end = '<?= date('t'); ?>'
          month_start = '<?= date('m', strtotime('-3 months')); ?>'
          month_end = '<?= date('m'); ?>'
          year_start = '<?= date('Y', strtotime('-3 months')); ?>'
          year_end = '<?= date('Y'); ?>'
          refresh()
          refreshFilter()
        } else if (filter == 'bulanan') {
          $('.filter-harian').hide();
          $('.filter-range').hide();
          $('.filter-bulanan').show();
          date_start = '01'
          date_end = '30'
          month_start = '<?= date('m'); ?>'
          month_end = '<?= date('m'); ?>'
          year_start = '<?= date('Y'); ?>'
          year_end = '<?= date('Y'); ?>'
          refresh();
          refreshFilter();
        } else if (filter == 'harian') {
          $('.filter-range').hide();
          $('.filter-bulanan').hide();
          $('.filter-harian').show();
          date_start = '<?= date('d'); ?>'
          date_end = '<?= date('d'); ?>'
          month_start = '<?= date('m'); ?>'
          month_end = '<?= date('m'); ?>'
          year_start = '<?= date('Y'); ?>'
          year_end = '<?= date('Y'); ?>'
          refresh();
          refreshFilter();
        }
      }
    })
  </script>