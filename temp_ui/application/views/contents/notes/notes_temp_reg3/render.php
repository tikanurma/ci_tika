<style>
  .mt-1 {
    margin-top: 1px;
  }

  .mt-2 {
    margin-top: 2px;
  }

  .mt-3 {
    margin-top: 3px;
  }

  .mt-4 {
    margin-top: 4px;
  }

  .mt-5 {
    margin-top: 5px;
  }

  .mt-10 {
    margin-top: 10px;
  }

  .mt-15 {
    margin-top: 15px;
  }

  .mt-20 {
    margin-top: 20px;
  }

  .mb-1 {
    margin-bottom: 1px;
  }

  .mb-2 {
    margin-bottom: 2px;
  }

  .mb-3 {
    margin-bottom: 3px;
  }

  .mb-4 {
    margin-bottom: 4px;
  }

  .mb-5 {
    margin-bottom: 5px;
  }

  .mb-10 {
    margin-bottom: 10px;
  }

  .ml-1 {
    margin-left: 1px;
  }

  .ml-2 {
    margin-left: 2px;
  }

  .ml-3 {
    margin-left: 3px;
  }

  .ml-4 {
    margin-left: 4px;
  }

  .ml-5 {
    margin-left: 5px;
  }

  .tindakan {
    background: #f5f5f5;
    border: 1px solid #e3e3e3;
    min-height: 100px;
    width: 100%;
    border-radius: 3px;
    padding-left: 5px;
  }

  .render-aktif {
    padding-left: 10px;
    padding-right: 10px;
  }
</style>


<div class="row mt-15">
  <div class="col-md-12">
    <div class="row">
      <?php if ($list) {; ?>

      <?php foreach ($list as $k => $v) : ?>
        <?php foreach ($v['data'] as $k) : ?>
          <div class="col-md-2 text-right">
            <button class="btn btn-primary btn-sm ubah-aktif" data-id-notes="<?= $k['notes_id']; ?>" data-id-notes-vitals="<?= $k['id_notes_vitals']; ?>"><i class="fa fa-fw fa-pencil"></i></button>
            <button class="btn btn-danger btn-sm hapus-aktif" data-id-notes="<?= $k['notes_id']; ?>" data-id-notes-vitals="<?= $k['id_notes_vitals']; ?>"><i class="fa fa-fw fa-trash"></i></button>
            <button class="btn btn-success btn-sm cetak-aktif" data-id-notes="<?= $k['notes_id']; ?>">Cetak</button>
            <!-- <div class="row text-center"><?= $k['notes_vitals'][0]['created_date']; ?></div> -->
          </div>
          <div class="col-md-10">
            <div class="row">
              <div class="col-lg-12">
                <h4><strong><?= $v['no_visit']; ?></strong></h4>
              </div>
            </div>
            <div class="row">
              <div class="col-md-4">
                <table class="table nowrap">
                  <tbody>
                    <tr>
                      <td>Dokter Approve</td>
                      <td><?= ucwords(strtolower($k['approved_dokter'])); ?></td>
                    </tr>
                    <tr>
                      <td>Petugas Approve</td>
                      <td><?= ucwords(strtolower($k['approved_petugas'])); ?></td>
                    </tr>
                    <tr>
                      <td>Data1</td>
                      <td><?= $k['data1']; ?></td>
                    </tr>
                    <tr>
                      <td>Data1</td>
                      <td><?= $k['data2']; ?></td>
                    </tr>
                    <tr>
                      <td>Tinggi Badan</td>
                      <td><?= $k['notes_vitals'][0]['height']; ?> cm</td>
                    </tr>
                    <tr>
                      <td>Berat Badan</td>
                      <td><?= $k['notes_vitals'][0]['weight']; ?> kg</td>
                    </tr>
                    <tr>
                      <td>Systolic</td>
                      <td><?= $k['notes_vitals'][0]['systolic']; ?> mmHg</td>
                    </tr>
                    <tr>
                      <td>Diastolic</td>
                      <td><?= $k['notes_vitals'][0]['diastolic']; ?> mmHg</td>
                    </tr>
                  </tbody>
                </table>
              </div>

              <div class="col-md-4">
                <table class="table nowrap">
                  <tbody>
                    <tr>
                      <td>Tekanan Darah</td>
                      <td><?= $k['notes_vitals'][0]['blood_pressure']; ?> mmHg</td>
                    </tr>
                    <tr>
                      <td>Pulse</td>
                      <td><?= $k['notes_vitals'][0]['pulse']; ?> x/menit</td>
                    </tr>
                    <tr>
                      <td>Pernafasan</td>
                      <td><?= $k['notes_vitals'][0]['respiratory_rate']; ?> x/menit</td>
                    </tr>
                    <tr>
                      <td>SPO2</td>
                      <td><?= $k['notes_vitals'][0]['spo2']; ?> %</td>
                    </tr>
                    <tr>
                      <td>Kesadaran</td>
                      <td><?= $k['notes_vitals'][0]['kesadaran']; ?></td>
                    </tr>
                    <tr>
                      <td>Systolic</td>
                      <td><?= $k['notes_vitals'][0]['keadaan_umum']; ?></td>
                    </tr>
                    <tr>
                      <td>Nyeri</td>
                      <td><?= $k['notes_vitals'][0]['nyeri']; ?></td>
                    </tr>
                    <tr>
                      <td>Eye Opening</td>
                      <td><?= $k['notes_vitals'][0]['eye_opening']; ?></td>
                    </tr>
                  </tbody>
                </table>
              </div>

              <div class="col-md-4">
                <table class="table nowrap">
                  <tbody>

                    <tr>
                      <td>Verbal Response</td>
                      <td><?= $k['notes_vitals'][0]['verbal_response']; ?></td>
                    </tr>
                    <tr>
                      <td>Motor Response</td>
                      <td><?= $k['notes_vitals'][0]['motor_response']; ?></td>
                    </tr>
                    <tr>
                      <td>Akral</td>
                      <td><?= $k['notes_vitals'][0]['akral']; ?></td>
                    </tr>
                    <tr>
                      <td>Reflek Cahaya</td>
                      <td><?= $k['notes_vitals'][0]['reflek_cahaya']; ?></td>
                    </tr>
                    <tr>
                      <td>Pupil Isokor</td>
                      <td><?= $k['notes_vitals'][0]['pupil_isokor']; ?></td>
                    </tr>
                    <tr>
                      <td>Pupil UnIsokor</td>
                      <td><?= $k['notes_vitals'][0]['pupil_unisokor']; ?></td>
                    </tr>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
          <hr>
        <?php endforeach; ?>
      <?php endforeach; ?>
    <?php } else {; ?>
      <div class="row">
        <div class="col-lg-3"></div>
        <div class="col-lg-9">
          <h4>Data belum diinput ! Silahkan tambah data terlebih dahulu.</h4>
        </div>
      </div>
    <?php }; ?>
    </div>
  </div>
</div>

<script type="text/javascript">
  $(document).ready(function() {
    $('#table-aktif').dataTable({
      // "paging": false,
      "searching": false,
      "scrollX": true,
      "info": false,
    });
    $('#table-aktif2').dataTable({
      "paging": false,
      "searching": false,
      "scrollX": true,
      "info": false,
      "ordering": false,
    });
  });

  $('.hapus-aktif').click(function(e) {
    e.preventDefault();

    if (confirm("Apa anda yakin ingin menghapus data ini?")) {


      delete_url = '<?php echo base_url(); ?>' + class_name + '/delete/';
      $('.hapus-aktif').attr('disabled', true);
      $.get('<?php echo base_url(); ?>' + class_name + '/delete/', {
        'id_notes': $(this).data('id-notes'),
        'id_notes_vitals': $(this).data('id-notes-vitals'),
      }).done(function(response) {
        data = JSON.parse(response);

        if (data.status == 200) {
          if (aktif_pdf) {
            aktif_pdf.close();
          }
          alert(data.message);
          render();
          return false;

        } else {
          alert(data.message);
          return false;

        }

      });
    } else {
      return false;
    }
  });

  $('.ubah-aktif').click(function(e) {
    e.preventDefault();
    id_reg = $('#id_pasien_registrasi').val(),
      process_button(0);
    $.get('<?php echo base_url(); ?>' + class_name + '/edit/', {
      'id_notes': $(this).data('id-notes'),
      'id_notes_vitals': $(this).data('id-notes-vitals'),
      'id_reg': id_reg
    }).done(function(response) {
      process_button(1);
      $('#form-container').html(response);
      $('#view-container').hide();
      if (!$('#form-grafik').is(':disabled')) {
        $('#form-grafik').attr("disabled", true)
      }
      $('#form-container').show();

    }).fail(function() {
      process_button(1);
      alert('Gagal menampilkan data')
    });
  });

  $('.cetak-aktif').on("click", function(e) {
    id_notes = $(this).data('id-notes');
    aktif_pdf = window.open('<?php echo base_url(); ?>' + class_name + '/view_pdf/' + id_notes);
  });
</script>
