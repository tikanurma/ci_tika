<a href="#" class="btn btn-primary btn-sm tambah pull-right">Tambah</a>
<br><br>
<table class="table nowrap datatables">
  <thead>
    <tr>
      <th>#</th>
      <th></th>
      <th>Id</th>
      <th>Occupation</th>
    </tr>
  </thead>
  <tbody>
    <?php $i=1; foreach ($occ as $d) : ?>
      <tr>
        <td><?= $i++; ?></td>
        <td>
          <button class="btn btn-sm btn-primary edit" data-id="<?= $d['id']; ?>">Edit</button>
        </td>
        <td><?= $d['id']; ?></td>
        <td><?= $d['occupation']; ?></td>
      </tr>
    <?php endforeach; ?>
  </tbody>
</table>
