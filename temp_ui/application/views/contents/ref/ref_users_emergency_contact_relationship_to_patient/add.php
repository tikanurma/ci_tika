<div class="col-lg-6">
	<div class="panel panel-primary">
		<div class="panel-heading">Form Tambah Relationship to Patient</div>
		<div class="panel-body">
			<form id="form-add" class="form-horizontal">
				
				<div class="form-group">
					<label class="control-label col-lg-3">Relationship to Patient:</label>
						<div class="col-lg-9">
							<input class="form-control" type="text" name="relationship_to_patient" placeholder="Relationship to patient " />	
				</div>
			</div>

				<div class="form-group">
					<div class="col-lg-offset-8 col-lg-10">
						<a href="#" class="btn btn-default back"> Batal</a>
						<button type="submit" class="btn btn-primary">Simpan</button>
					</div>
				</div>
			</form>
		</div>
	</div>
</div>

<script>
	$('#form-add').submit(function(e) {
		e.preventDefault();
		data = $(this).serialize();
		$.post('<?= base_url() ?>' + class_name + '/add_process', data, function(data) {
			var data = JSON.parse(data);
			if (data.status == '200') {
				alert(data.message);
				location.reload();
			} else {
				alert('Gagal mengirim data.');
			}
		});
	});

</script>
