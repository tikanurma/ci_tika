<a href="#" class="btn btn-primary btn-sm tambah pull-right">Tambah</a>
<br><br>
<table class="table nowrap datatables">
  <thead>
    <tr>
      <th>#</th>
      <th>Id</th>
      <th>Blood Type</th>
    </tr>
  </thead>
  <tbody>
    <?php $i=1; foreach ($blood as $d) : ?>
      <tr>
        <td><?= $i++; ?></td>
        <td><?= $d['id']; ?></td>
        <td><?= $d['blood']; ?></td>
      </tr>
    <?php endforeach; ?>
  </tbody>
</table>
