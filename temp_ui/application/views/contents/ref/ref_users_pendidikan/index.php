<br> <br>
<div class="load">

</div>


<script type="text/javascript">
    const class_name = '<?=$class_name;?>';
    $(document).ready(function() {
        $.post('<?php echo base_url(); ?>' + class_name  + '/list', {
                id: ''
            },
            function(html) {
                $(".load").html(html);
                $("table.datatables").DataTable();
            }
        );

    });

    $(document).on('click', '.tambah', function(e) {
        e.preventDefault();
        $.post('<?php echo base_url() ?>' + class_name  + '/add',
            function(html) {
                $(".load").html(html);
            }
        );
    });

    $(document).on('click', '.edit', function(e) {
        e.preventDefault();
        $.post('<?php echo base_url() ?>' + class_name  + '/edit', {
                id: $(this).data('id')
            },
            function(html) {
                $(".load").html(html);
            }
        );
    });

    $(document).on('click', '.delete', function(e) {
        e.preventDefault();
        var r = confirm("Yakin hapus data auto_tarif ini ?");
        if (r == true) {
            $.post('<?php echo base_url() ?>' + class_name  + '/delete', {
                id: $(this).data('id')
            }, function(data) {
                var data = JSON.parse(data);
                if (data.status == '200') {
                    alert(data.message);
                    location.reload();
                } else {
                    alert('Gagal mengirim data.');
                }
            });
        } else {
            return false;
        }
    });

    $(document).on('click', '.back', function(e) {
        e.preventDefault();
        $.post('<?php echo base_url() ?>' + class_name  + '/list',
            function(html) {
                $(".load").html(html);
                $("table.datatables").DataTable();
            }
        );
    });
</script>
