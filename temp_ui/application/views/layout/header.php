<div class="load-nav">

        <nav class="navbar navbar-default navbar-fixed-top" id="focus_div">
          <div class="container-fluid">
            <ul class="nav navbar-nav">
              <li>
                <a class="navbar-brand" href="#" data-toggle="tooltip" title="Home"><span class="fa fa-home"></span></a>
              </li>
              <li><a href="#"><strong>FREELANCER WORK IN PROGRESS</strong></a></li>
              <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                HOME <span class="caret"></span></a>
                <ul class="dropdown-menu scrollable-menu" role="menu">
                  <li class="active"> <a href="#">HOME</a></li>
                </ul>
              </li>
            </ul>

            <ul class="nav navbar-nav navbar-right hover-primary" style="margin-right: 15px;">
              <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Tony Stark <span class="caret"></span></a>
                <ul class="dropdown-menu">
                  <li><a href="#">Edit Profile</a></li>
                  <li class="divider"></li>
                  <li><a href="#">xxxx</a></li>
                  <li class="divider"></li>
                  <li><a href="#">Admin</a></li>
                </ul>
              </li>
              <a class="btn btn-default" href="#" style="margin:8px 22px 0px 15px;">Logout</a>
            </ul>
          </div>
        </nav>


        <!-- MENUBAR -->
        <div class="menubar" id="nav">
        <a class="nav menu <?php if($this->uri->segment(1) == 'notes_temp'){echo 'selected';} ?>" href="<?php echo base_url('laporan_temp');?>">LAPORAN TEMPLATE</a>
      </div>

      <!-- MENUBAR 2 -->
      <div class="menubar2" id="sub_menu_refrensi">

        <a class="nav menu <?php if($this->uri->segment(1) == 'laporan_temp'){echo 'black-selected';} ?>" href="<?php echo base_url('laporan_temp');?>">LAPORAN_TEMP</a>
        <a class="nav menu <?php if($this->uri->segment(1) == 'notes_temp_reg3'){echo 'black-selected';} ?>" href="<?php echo base_url('notes_temp_reg3');?>">NOTES TEMP V3</a>
        <a class="nav menu <?php if($this->uri->segment(1) == 'crud_temp'){echo 'black-selected';} ?>" href="<?php echo base_url('crud_temp');?>">CRUD TEMPLATE</a>
        <a class="nav menu <?php if($this->uri->segment(1) == 'suku_bangsa'){echo 'black-selected';} ?>" href="<?php echo base_url('ref_suku_bangsa');?>">Suku Bangsa</a>
        <a class="nav menu <?php if($this->uri->segment(1) == 'users_blood_type'){echo 'black-selected';} ?>" href="<?php echo base_url('ref_users_blood_type');?>">Blood Type</a>
        <a class="nav menu <?php if($this->uri->segment(1) == 'users_occupation'){echo 'black-selected';} ?>" href="<?php echo base_url('ref_users_occupation');?>">User Occupation</a>
        <a class="nav menu <?php if($this->uri->segment(1) == 'users_pendidikan'){echo 'black-selected';} ?>" href="<?php echo base_url('ref_users_pendidikan');?>">User Pendidikan</a>
        <a class="nav menu <?php if($this->uri->segment(1) == 'relationship_to_patient'){echo 'black-selected';} ?>" href="<?php echo base_url('ref_users_emergency_contact_relationship_to_patient');?>">Relationship</a>
        <a class="nav menu <?php if($this->uri->segment(1) == 'rekam_medis'){echo 'black-selected';} ?>" href="<?php echo base_url('ref_rekam_medis_category');?>">Rekam Medis Category</a>      
      </div>

      </div>
