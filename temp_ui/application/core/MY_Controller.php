<?php defined('BASEPATH') OR exit('No direct script access allowed');

use GuzzleHttp\Psr7;
use \GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Exception\ServerException;

/**
 * MY controller
 *
 * Seluruh controller bisa extends ke MY_Controller.
 * Opsi ini akan menghemat pengkodingan terkait dengan penggunaan Guzzle
 * 
 * @extends RestController
 */
class MY_Controller extends CI_Controller
{
	/**
	 * Token
	 *
	 * Bisa diganti atau generate baru jika sudah expired (dev mode)
	 * 
	 * @var string
	 * @access protected
	 */
	protected $_token = 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJuYW1lIjoiTkFNQSAyIiwiaWRfdXNlciI6IjM3MzY4MSIsInJtX251bWJlciI6ImFkbWluIiwicnNfa2V5IjoiQTEyMyIsImlwX2FkZHJlc3MiOiIxMjcuMC4xLjEiLCJhY2Nlc3MiOiJ1c2VyIn0.ubW6fyc7ErYOW2T5qFbjXvLIVTLp05s3A0paQ6wfcmo';
	
	/**
	 * Guzzle client class
	 * 
	 * @var object
	 * @access protected
	 */
	protected $_client;
	
	/**
	 * Data user login
	 * 
	 * @var array
	 * @access protected
	 */
	protected $_user_login = [
		'name' => 'NAMA 2',
		'id_user' => '373681',
		'rm_number' => 'admin',
		'rs_key' => 'A123',
		'ip_address' => '127.0.1.1',
		'access' => 'user'
	];
	
	/**
	 * Temp folder
	 * 
	 * @var string
	 * @access protected
	 */
	protected $_temp_folder;
	
	/**
	 * Constructor
	 * 
	 * @access public
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
		
		// set guzzle client
		$this->_client = new Client([
			'base_uri' => $this->config->item('api_url'),
			'headers' => [
				'Content-Type' => 'application/json',
				'x-token' => $this->_token
			]
		]);
		
		// ambil user login data di session jika tersedia
		if ($this->session->userdata('user_login')) {
			$this->_user_login = $this->session->userdata('user_login');
		}
		
		// menghilangkan 'C_' pada nama class untuk dinamisasi routing
		$this->class = str_replace('c_', '', strtolower($this->router->fetch_class()));
		
		// set temp folder yang bisa digunakan untuk semua controller
		if (strpos($_SERVER['DOCUMENT_ROOT'], 'xampp') == true) {
			$this->_temp_folder = str_replace('/htdocs', '', $_SERVER['DOCUMENT_ROOT']) . '/tmp/';
		} else {
			$this->_temp_folder = '/tmp/';
		}
	}
}