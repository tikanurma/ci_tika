<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class M_jadwal_operasi extends CI_Model {

    public function jadwal_operasi_get($keyword = NULL)
	{
		$this->db->select("
		a.kodebooking AS kode_booking,
		d.payment AS penjamin,
		b.user_id AS id_users_pasien,
		b.full_name AS nama_pasien,
		e.full_name AS nama_dokter
		");
		$this->db->from('rs_jadwal_operasi a');
		$this->db->join('users_profile b', 'a.id_users_pasien = b.user_id', 'left');
        $this->db->join('dept_ruang_operasi c', 'a.id_dept_ruang_operasi = c.id', 'left');
		$this->db->join('ref_payment d', 'd.id_ref_payment = a.id_ref_payment', 'left');
		$this->db->join('users_profile e', 'a.id_users_dokter = e.user_id', 'left');

		$query = $this->db->get()->result_array();
		// $query = $this->db->get_compiled_select();

		return $query;
	}
}

// FROM rs_jadwal_operasi a
// LEFT JOIN users_profile b ON a.id_users_pasien = b.user_id
// LEFT JOIN dept_ruang_operasi c ON a.id_dept_ruang_operasi = c.id
// LEFT JOIN ref_payment d ON d.id_ref_payment = a.id_ref_payment
// LEFT JOIN users_profile e ON a.id_users_dokter = e.user_id