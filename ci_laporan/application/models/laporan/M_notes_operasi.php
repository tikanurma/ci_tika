<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class M_notes_operasi extends CI_Model {

    public function notes_operasi_get($keyword = NULL)
	{
		$this->db->select("
		d.full_name nama_pasien,
		d.no_rm no_rm,
		d.bpjs_id no_bpjs,
		d.number_id no_ktp,
		d.gender kelamin,
		a.id id,
		a.id_pasien_visit id_visit,
		a.dokter_operator dokter_operator,
		a.dokter_anastesi dokter_anastesi,
		a.asisten_1 asisten_1,
		a.asisten_2 asisten_2,
		a.perawatan_anastesi perawatan_anastesi,
		a.jenis_spesialis jenis_spesialis,
		a.jenis_tindakan_pembedahan jenis_tindakan_pembedahan,
		a.golongan_operasi golongan_operasi,
		a.diagnosa_utama_pra_operasi diagnosa_utama_pra_operasi,
		a.cito_elektif cito_elektif,
		a.golongan_anastesi golongan_anastesi,
		a.asal_pasien asal_pasien,
		a.start_operasi start_operasi,
		a.end_operasi end_operasi,
		a.jenis_anastesi jenis_anastesi,
		a.diagnosa_pra_operasi diagnosa_pra_operasi,
		a.diagnosa_pasca_operasi diagnosa_pasca_operasi,
		a.approved_by approved_by,
		a.approved_date approved_date,
		a.komplikasi komplikasi,
		a.is_pendarahan is_pendarahan,
		a.ml_pendarahan ml_pendarahan,
		a.ml_wb ml_wb,
		a.ml_prc ml_prc,
		a.ml_ffp ml_ffp,
		a.ml_cryo ml_cryo,
		a.is_kultur is_kultur,
		a.is_jaringan_ke_patologi is_jaringan_ke_patologi,
		a.date_jaringan_ke_patologi date_jaringan_ke_patologi,
		a.desc_jaringan_ke_patologi desc_jaringan_ke_patologi,
		a.rencana_pasca_operasi rencana_pasca_operasi,
		a.instruksi_pasca_bedah_diet_puasa instruksi_pasca_bedah_diet_puasa,
		a.instruksi_pasca_bedah_diet_asupan instruksi_pasca_bedah_diet_asupan,
		a.instruksi_pasca_bedah_diet_feeding instruksi_pasca_bedah_diet_feeding,
		a.instruksi_pasca_bedah_ivfd instruksi_pasca_bedah_ivfd,
		a.instruksi_pasca_bedah_obat instruksi_pasca_bedah_obat,
		a.uraian_operasi uraian_operasi
		
		");
		$this->db->from('notes_operasi a');
		$this->db->join('pasien_visit b', 'a.id_pasien_visit = b.id_pasien_visit', 'left');
		$this->db->join('pasien_registrasi c', 'c.id_pasien_registrasi = b.id_pasien_registrasi', 'left');
    $this->db->join('users_profile d', 'd.user_id = c.id_users_pasien', 'left');
		// $this->db->join('ref_payment d', 'd.id_ref_payment = a.id_ref_payment', 'left');
		// $this->db->join('users_profile e', 'a.id_users_dokter = e.user_id', 'left');
		
		$x = [
			'del_date' => null
			// 'is_del' => '0'
			];
		$this->db->where($x);
		
		$query = $this->db->get()->result_array();

		return $query;
	}
}