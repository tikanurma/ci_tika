<?php

class M_laporan_temp extends CI_Model
{
  public function __construct()
  {
    parent::__construct();
  }

  public function get($filter)
  {
    /*
    Selalu gunakan alias.
    Contoh
      $this->db->select("a.id id");
      $this->db->from("table1 a");
    Jika perlu JOIN ke table laen. Untuk default selalu gunakan LEFT JOIN,
    KECUALI untuk kasus-kasus tertentu silahkan gunakan yang lain.
    Untuk hasil get database nya selalu berbentuk ARRAY. JANGAN BERBENTUK OBJEK
     $this->db->get()->result_array();
    
    TIDAK BOLEH :
      $this->db->get()->row_array();
      $this->db->get()->result();
    */
    $this->db->select('
            a.id_pasien_registrasi id_reg,
            a.no_reg no_reg,
            a.code_icd10_awal as code_icd10_awal,
            l.nama_icd10  nama_icd10_awal,
            a.checkin_time tanggal_checkin,
            a.checkin_petugas nama_petugas_checkin,
            a.checkout_time tanggal_checkout,
            a.checkout_petugas nama_petugas_checkout,
            a.kunjungan_ke_berapa total_kunjungan,
            a.is_pasien_baru,
            a.lama_rawat,

            b.dob tanggal_lahir,
            b.gender kelamin,
            b.full_name nama_pasien,
            b.bpjs_id no_bpjs,
            b.address alamat1,
            b.no_rm no_rm,

            c.id as id_kelurahan,
            c.nama_kelurahan as nama_kelurahan,
            d.id as id_kecamatan,
            d.nama_kecamatan as nama_kecamatan,
            e.nama_kabupaten,
            f.nama_provinsi,

            i.cara_datang asal_pasien,

            g.payment as payment,
            g.id_ref_payment as id_penjamin,
            h.level,
            h.nama alasan_pulang,
            j.kondisi kondisi_pulang,

            k.full_name nama_dpjp,
            a.id_hrd_dokter id_dpjp,

            fisik.pemeriksaan_fisik pemeriksaan_fisik,
            penunjang.pemeriksaan_penunjang pemeriksaan_penunjang ,
            riwayat_penyakit.catatan diagnosa_dokter,
            terapi_tindakan.terapi_tindakan as terapi_tindakan,

            a.jenis_rawat jenis_rawat ,
            m.departement_name ruang_rawat,
            m.departement_id as id_departement,

            icd_10_primary.code_icd10 code_icd10_primary,
            icd_10_primary.nama_icd10 nama_icd10_primary,
            icd_9.code_icd9 code_icd9,
            icd_9.nama_icd9 nama_icd9,
            icd_10_secondary.code_icd10 code_icd10_secondary,
            icd_10_secondary.nama_icd10 nama_icd10_secondary,

            n.noSep as no_sep,
            n.noKartu as no_kartu,
            n.nama as nama_sep,
            n.tglSep as tgl_sep,
            n.tglPulang as tgl_pulang,
            n.ppkPelayanan as ppk_pelayanan,
            n.jnsPelayanan as jns_pelayanan,
            n.klsRawat as kls_rawat,
            n.jenis_rawat as jenis_rawat,
            n.noMr as no_mr,
            n.catatan as catatan,
            n.code_diagAwal as code_diagnosa_awal,
            n.diagAwal as diagnosa_awal,
            n.poliTujuan as poli_tujuan,
            n.code_poliTujuan as code_poli_tujuan,
            n.noSurat as no_surat,
            n.kodeDPJP as kode_dpjp,
            n.nama_dpjp as nama_dpjp,
            n.noTelp as no_telp,
            n.created_date,
            n.deleted_date,

            o.noRujukan as no_rujukan,
            o.tglRujukan as tgl_rujukan,
            o.tglSelesaiRujukan as tgl_selesai_rujukan,
            o.kdPpk as kd_ppk,
            o.nmPpk as nm_ppk,
            o.jnsPelayanan as jns_pelayanan,
            o.catatan as catatan,
            o.kdDiag as kd_diagnosa,
            o.nmDiag as nm_diagnosa,
            o.kdPoli as kd_poli,
            o.nmPoli as nm_poli,
            o.nama as nama,
            o.noKartu as no_kartu,
            o.id_petugas_input as id_petugas_input,
            o.nama_petugas_input as nama_petugas_input
        ');

    $this->db->from('pasien_registrasi a');
    $this->db->join('users_profile b', 'a.id_users_pasien = b.user_id', 'left');
    $this->db->join('ref_address_kelurahan c', 'b.id_kel = c.id', 'left');
    $this->db->join('ref_address_kecamatan d', 'c.id_ref_address_kecamatan = d.id', 'left');
    $this->db->join('ref_address_kabupaten e', 'd.id_ref_address_kabupaten = e.id', 'left');
    $this->db->join('ref_address_provinsi f', 'e.id_ref_address_provinsi = f.id', 'left');
    $this->db->join('ref_payment g', 'a.id_ref_payment = g.id_ref_payment', 'left');
    $this->db->join('ref_checkout h', 'a.id_ref_checkout = h.id', 'left');
    $this->db->join('ref_cara_datang i', 'a.id_ref_cara_datang = i.id_ref_cara_datang', 'left');
    $this->db->join('ref_kondisi j', 'a.id_ref_kondisi = j.id_ref_kondisi', 'left');
    $this->db->join('users_profile k', 'a.id_hrd_dokter = k.user_id', 'left');
    $this->db->join('ref_icd10 l', 'a.code_icd10_awal = l.code_icd10', 'left');
    $this->db->join('departements m', 'a.id_dept_ruang_rawat = m.departement_id', 'left');
    $this->db->join('vclaim_sep n', 'n.id_pasien_registrasi = a.id_pasien_registrasi', 'left');
    $this->db->join('vclaim_rujukan o', 'o.noSep = n.noSep', 'left');

    $this->db->join('(
      SELECT nr.id_pasien_registrasi, GROUP_CONCAT(DISTINCT nrpf.pemeriksaan_fisik SEPARATOR \', \') pemeriksaan_fisik
      FROM notes_resume nr
      LEFT JOIN notes_resume_pemeriksaan_fisik nrpf ON nrpf.id_notes_resume = nr.id
      WHERE nrpf.del_date is null
      GROUP BY nr.id_pasien_registrasi
      ORDER BY nr.id DESC
    ) as fisik', 'fisik.id_pasien_registrasi = a.id_pasien_registrasi', 'left');

    $this->db->join('(
      SELECT nr.id_pasien_registrasi, GROUP_CONCAT(DISTINCT nrpp.pemeriksaan_penunjang SEPARATOR \', \') pemeriksaan_penunjang
      FROM notes_resume nr
      LEFT JOIN notes_resume_pemeriksaan_penunjang nrpp ON nrpp.id_notes_resume = nr.id
      WHERE nrpp.del_date is null
      GROUP BY nr.id_pasien_registrasi
      ORDER BY nr.id DESC
    ) penunjang', 'penunjang.id_pasien_registrasi = a.id_pasien_registrasi', 'left');

    $this->db->join('(
      SELECT nr.id_pasien_registrasi, GROUP_CONCAT(DISTINCT nrrrp.catatan SEPARATOR \', \') catatan
      FROM notes_resume nr
      LEFT JOIN notes_resume_ringkasan_riwayat_penyakit nrrrp ON nrrrp.id_notes_resume = nr.id
      WHERE nrrrp.del_date is null
      GROUP BY nr.id_pasien_registrasi
      ORDER BY nr.id DESC
    ) riwayat_penyakit', 'riwayat_penyakit.id_pasien_registrasi = a.id_pasien_registrasi', 'left');

    $this->db->join('(
      SELECT nr.id_pasien_registrasi, GROUP_CONCAT(DISTINCT nrtt.terapi_tindakan SEPARATOR \', \') terapi_tindakan
      FROM notes_resume nr
      LEFT JOIN notes_resume_terapi_tindakan nrtt ON nrtt.id_notes_resume = nr.id
      WHERE nrtt.del_date is null
      GROUP BY nr.id_pasien_registrasi
      ORDER BY nr.id DESC
    ) terapi_tindakan', 'terapi_tindakan.id_pasien_registrasi = a.id_pasien_registrasi', 'left');

    $this->db->join('(
      SELECT id_pasien_registrasi, group_concat(code_icd10 SEPARATOR \', \') code_icd10, group_concat(nama_icd10 SEPARATOR \' | \') nama_icd10
      FROM pasien_visit pv
      JOIN(
        SELECT ncid10.id_visit, ricd10.code_icd10, ricd10.nama_icd10
        FROM notes_icd10 ncid10
        JOIN ref_icd10 ricd10 ON ncid10.code_icd10 = ricd10.code_icd10
        WHERE ncid10.xprimary = 1
      )ax on ax.id_visit = pv.id_pasien_visit
      WHERE del_date is null
      GROUP BY id_pasien_registrasi
    ) icd_10_primary', 'icd_10_primary.id_pasien_registrasi = a.id_pasien_registrasi', 'left');

    $this->db->join('(
      SELECT id_pasien_registrasi, group_concat(code_icd9 SEPARATOR \', \') code_icd9 , group_concat(nama_icd9 SEPARATOR \' | \') nama_icd9
      FROM pasien_visit pv2
      JOIN (
        SELECT ncid9.id_visit,ricd9.code_icd9, ricd9.nama_icd9
        FROM notes_icd9 ncid9
        JOIN ref_icd9 ricd9 ON ncid9.code_icd9 = ricd9.code_icd9
      ) bx on bx.id_visit = pv2.id_pasien_visit
      WHERE pv2.del_date is null
      GROUP BY pv2.id_pasien_registrasi
    ) icd_9', 'icd_9.id_pasien_registrasi = a.id_pasien_registrasi', 'left');

    $this->db->join('(
      SELECT id_pasien_registrasi, group_concat(code_icd10 SEPARATOR \', \') code_icd10, group_concat(nama_icd10 SEPARATOR \' | \') nama_icd10
      FROM pasien_visit pv3
      JOIN(
        SELECT nicd10.id_visit, ricd10.code_icd10, ricd10.nama_icd10
        FROM notes_icd10 nicd10
        JOIN ref_icd10 ricd10 ON nicd10.code_icd10 = ricd10.code_icd10
        WHERE nicd10.xprimary <> 1
      ) ax on ax.id_visit = pv3.id_pasien_visit
      WHERE pv3.del_date is null
      GROUP BY pv3.id_pasien_registrasi
    ) icd_10_secondary', 'icd_10_secondary.id_pasien_registrasi = a.id_pasien_registrasi', 'left');

    /*
    Proses filter sesuai dengan request client.
    Silahkan disesuaikan dengan tugas masing-masing.
    Silahkan hapus jika tidak perlukan.
    */
    if ($filter['jenis_periode'] == 1 && ($filter['periode_end'] . ' 23:59:59' > $filter['periode_start'])) {
      $this->db->where('a.checkin_time >=', $filter['periode_start']);
      $this->db->where('a.checkin_time <=', $filter['periode_end'] . ' 23:59:59');
    } elseif ($filter['jenis_periode'] == 2 && ($filter['periode_end'] . ' 23:59:59' > $filter['periode_start'])) {
      $this->db->where('a.checkout_time >=', $filter['periode_start']);
      $this->db->where('a.checkout_time <=', $filter['periode_end'] . ' 23:59:59');
    }

    if (isset($filter['id_reg']) && !empty($filter['id_reg'])) {
      $this->db->where('a.id_pasien_registrasi', $filter['id_reg']);
    }

    if (isset($filter['is_reg_del_date_null']) && $filter['is_reg_del_date_null'] == 1) {
      $this->db->where('a.del_date IS NULL');
    }

    if (isset($filter['is_reg_del_date_null']) && $filter['is_reg_del_date_null'] == 2) {
      $this->db->where('a.del_date IS NOT NULL');
    }

    if (isset($filter['jenis_visit']) && !empty($filter['jenis_visit'])) {
      $this->db->where('a.jenis_rawat', $filter['jenis_visit']);
    }

    if (isset($filter['icd10']) && !empty($filter['icd10'])) {
      $this->db->where('icd_10_primary.code_icd10', $filter['icd10']);
    }

    if (isset($filter['no_rm']) && !empty($filter['no_rm'])) {
      $this->db->where('b.no_rm', $filter['no_rm']);
    }

    if (isset($filter['is_pasien_baru']) && $filter['is_pasien_baru'] == 1) {
      $this->db->where('a.is_pasien_baru', 0);
    }

    if (isset($filter['is_pasien_baru']) && $filter['is_pasien_baru'] == 2) {
      $this->db->where('a.is_pasien_baru', 1);
    }

    if (isset($filter['gender']) && !empty($filter['gender'])) {
      $this->db->where('b.gender', $filter['gender']);
    }

    if (isset($filter['id_ref_payment']) && !empty($filter['id_ref_payment'])) {
      $this->db->where('g.id_ref_payment', $filter['id_ref_payment']);
    }

    if (isset($filter['id_dpjp']) && !empty($filter['id_dpjp'])) {
      $this->db->where('k.user_id', $filter['id_dpjp']);
    }

    if (isset($filter['id_dept_ruang_rawat']) && !empty($filter['id_dept_ruang_rawat'])) {
      $this->db->where('m.departement_id', $filter['id_dept_ruang_rawat']);
    }

    if (isset($filter['id_kel']) && !empty($filter['id_kel'])) {
      $this->db->where('c.id', $filter['id_kel']);
    }

    if (isset($filter['id_kec']) && !empty($filter['id_kec'])) {
      $this->db->where('d.id', $filter['id_kec']);
    }
    /* End of filter */

    /*
    Fitur search dari datatables. Silahkan disesuaikan dengan kebutuhan.
    Yang mana bisa disearch silahkan disesuaikan
    */
    if (isset($filter['search']) && !empty($filter['search'])) {
      $this->db->group_start()
        ->like('k.full_name', $filter['search'])
        ->or_like('b.full_name', $filter['search'])
        ->or_like('c.nama_kelurahan', $filter['search'])
        ->or_like('d.nama_kecamatan', $filter['search'])
        ->or_like('m.departement_name', $filter['search'])
        ->or_like('i.cara_datang', $filter['search'])
        ->or_like('g.payment', $filter['search'])
        ->or_like('b.no_rm', $filter['search'])
        ->or_like('b.bpjs_id', $filter['search'])
        ->or_like('b.full_name', $filter['search'])
        ->or_like('b.address', $filter['search'])
        ->or_like('a.is_pasien_baru', $filter['search'])
        ->or_like('h.nama', $filter['search'])
        ->or_like('j.kondisi', $filter['search'])
        ->or_like('m.departement_name', $filter['search'])
        ->or_like('icd_10_primary.nama_icd10', $filter['search'])
        ->or_like('icd_10_primary.code_icd10', $filter['search'])
        ->or_like('icd_10_secondary.nama_icd10', $filter['search'])
        ->or_like('icd_10_secondary.code_icd10', $filter['search'])
        ->group_end();
    }

    return $this->db->get()->result_array();
  }

  public function getDataShiftById($id)
  {
    $this->db->select("
      a.id id,
      a.nama_shift nama_shift,
      a.time_start time_start,
      a.time_end time_end
    ");
    $this->db->from('master_hr_shift a');
    $this->db->where('a.id', $id);
    return $this->db->get()->row_array();
  }

  public function getDataStatistik($filter)
  {
    $this->db->select("
      a.is_pasien_baru status_pasien,
      a.checkin_time checkin_time,
      a.checkout_time checkout_time,

      b.dob tanggal_lahir,
      b.gender kelamin,

      c.full_name nama_dokter,
      c.user_id id_dokter,

      a.id_dept_ruang_rawat id_dept_ruang_rawat,
      d.departement_name nama_dept_ruang_rawat,

      e.id as id_kelurahan,
      e.nama_kelurahan nama_kelurahan,

      f.id as id_kecamatan,
      f.nama_kecamatan as nama_kecamatan,

      a.id_ref_payment id_penjamin,
      g.payment as penjamin,


    ");
    $this->db->from('pasien_registrasi a');
    $this->db->join('users_profile b', 'a.id_users_pasien = b.user_id', 'left');
    $this->db->join('users_profile c', 'a.id_hrd_dokter = c.user_id', 'left');
    $this->db->join('departements d', 'a.id_dept_ruang_rawat = d.departement_id', 'left');
    $this->db->join('ref_address_kelurahan e', 'b.id_kel = e.id', 'left');
    $this->db->join('ref_address_kecamatan f', 'e.id_ref_address_kecamatan = f.id', 'left');
    $this->db->join('ref_payment g', 'a.id_ref_payment = g.id_ref_payment', 'left');

    if ($filter['jenis_periode'] == 1 && ($filter['periode_end'] . ' 23:59:59' > $filter['periode_start'])) {
      $this->db->where('a.checkin_time >=', $filter['periode_start']);
      $this->db->where('a.checkin_time <=', $filter['periode_end'] . ' 23:59:59');
    } elseif ($filter['jenis_periode'] == 2 && ($filter['periode_end'] . ' 23:59:59' > $filter['periode_start'])) {
      $this->db->where('a.checkout_time >=', $filter['periode_start']);
      $this->db->where('a.checkout_time <=', $filter['periode_end'] . ' 23:59:59');
    }

    if (isset($filter['gender']) && $filter['gender'] != null) {
      $this->db->where('b.gender', $filter['gender']);
    }

    if (isset($filter['id_dpjp']) && $filter['id_dpjp'] != null) {
      $this->db->where('c.user_id', $filter['id_dpjp']);
    }

    if (isset($filter['is_pasien_baru']) && $filter['is_pasien_baru'] != null) {
      $this->db->where('a.is_pasien_baru', $filter['is_pasien_baru']);
    }

    if (isset($filter['id_kel']) && $filter['id_kel'] != null) {
      $this->db->where('e.id_kel', $filter['id_kel']);
    }

    if (isset($filter['id_kec']) && $filter['id_kec'] != null) {
      $this->db->where('f.id_kec', $filter['id_kec']);
    }

    if (isset($filter['id_ref_payment']) && $filter['id_ref_payment'] != null) {
      $this->db->where('g.id_ref_payment', $filter['id_ref_payment']);
    }

    if (isset($filter['id_dept_ruang_rawat']) && $filter['id_dept_ruang_rawat'] != null) {
      $this->db->where('a.id_dept_ruang_rawat', $filter['id_dept_ruang_rawat']);
    }

    if (isset($filter['jenis_rawat']) && $filter['jenis_rawat'] != null) {
      $this->db->where('a.jenis_rawat', $filter['jenis_rawat']);
    }
    $this->db->where('a.del_date is null');
    return $this->db->get()->result_array();
  }
}
