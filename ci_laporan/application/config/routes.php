<?php
defined('BASEPATH') or exit('No direct script access allowed');

$route['default_controller'] = 'welcome';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;

/// Route for laporan_temp
$route['laporan_temp'] = 'laporan/c_laporan_temp';
$route['laporan_temp/statistik_registrasi'] = 'laporan/c_laporan_temp/statistik_registrasi';