<?php
defined('BASEPATH') or exit('No direct script access allowed');

class C_laporan_temp extends MY_Controller
{
  /*
  List parameter yang boleh dikirim untuk endpoint . Jika parameter 
  yang dikirim ke endpoint tidak ada dalam list ini,  maka akan muncul error. 
  Silahkan disesuaikan dengan kebutuhan. Untuk template ini, saya menggunakan
  dua endpoint yang membutuhkan parameter yang berbeda.
  */
  private $param1 = [
    'jenis_periode',
    'periode_start',
    'periode_end',
    'id_reg',
    'is_reg_del_date_null',
    'jenis_visit',
    'umur_start',
    'umur_end',
    'icd10',
    'no_rm',
    'is_pasien_baru',
    'gender',
    'jenis_rawat',
    'id_dept_ruang_rawat',
    'id_ref_payment',
    'id_dpjp',
    'id_kec',
    'id_kel',
    'shift',
    'search',
    'offset',
    'limit'
  ];

  private $param2 = [
    'jenis_periode',
    'periode_start',
    'periode_end',
    'is_pasien_baru',
    'is_reg_del_date_null',
    'gender',
    'id_ref_payment',
    'id_dpjp',
    'id_kec',
    'id_kel',
    'id_dept_ruang_rawat',
    'jenis_rawat'
  ];

  function __construct()
  {
    parent::__construct();
    /*
    Load model. Selalu gunakan alias untuk model. Jika hanya load 1 model,
    disarankan menggunakan alias 'model'. Jika lebih dr satu model disarankan
    alias menggunakan nama model.
    Contoh :
      $this->load->model('laporan/M_stock', 'stock');
    */
    $this->load->model('laporan/M_laporan_temp', 'model');

    /*
    List variable output untuk endpoint statistik_registrasi. Sesuaikan dengan
    kebutuhan. Jika tidak diperlukan silahkan dihapus saja.  
    */
    $this->arrDays6 = [
      'pria' => 0,
      'wanita' => 0
    ];

    $this->arrDays28 = [
      'pria' => 0,
      'wanita' => 0
    ];

    $this->arrDays360 = [
      'pria' => 0,
      'wanita' => 0
    ];

    $this->arrYear4 = [
      'pria' => 0,
      'wanita' => 0
    ];

    $this->arrYear14 = [
      'pria' => 0,
      'wanita' => 0
    ];

    $this->arrYear24 = [
      'pria' => 0,
      'wanita' => 0
    ];

    $this->arrYear44 = [
      'pria' => 0,
      'wanita' => 0
    ];

    $this->arrYear64 = [
      'pria' => 0,
      'wanita' => 0
    ];

    $this->arrYear65 = [
      'pria' => 0,
      'wanita' => 0
    ];

    $this->dataKecamatan = [];

    $this->dataKelurahan = [];

    $this->dataGender = [
      'pria' => 0,
      'wanita' => 0
    ];

    $this->statusPasien = [
      'lama' => 0,
      'baru' => 0
    ];

    $this->dataPenjamin = [];

    $this->dataDokter = [];

    $this->dataDeptRuangRawat = [];
  }

  /*
  Function untuk check apakah format date yang diinput valid 
  atau tidak.
  */
  public function valid_date($date)
  {
    $d = DateTime::createFromFormat('Y-m-d', $date);
    return $d && $d->format('Y-m-d') === $date;
  }

  /*
  Endpoint laporan_temp
  $route['laporan_temp'] = 'laporan/c_laporan_temp';
  */
  public function index_get()
  {
    $filter = $this->get();
    $this->load->library('form_validation');

    /*
    Cek apakah parameter yang dikirim ke endpoint diperbolehkan
    atau tidak. Jika parameter yang dikirim tidak ada di list param1,
    maka akan direspon error 500. Tidak ada yang perlu diubah disini.
    */
    foreach ($filter as $key => $val) {
      if (!in_array($key, $this->param1)) {
        $res['status'] = 500;
        $res['message'] = "Error ! Parameter '" .  $key . "' tidak diperbolehkan !!!";
        $this->response($res, $res['status']);
      }
    }

    /*
    Validasi untuk masing-masing parameter yang digunakan di endpoint ini.
    Tolong untuk masing-masing rules, diset error message nya. Beberapa contoh error message
    dapat dilihat dari contoh dibawah ini. Ini adalah form validation standard dari CodeIgniter.
    */
    $this->form_validation->set_data($filter);

    $this->form_validation->set_rules('jenis_periode', 'Jenis Periode', 'required|numeric|in_list[1,2]', [
      'required' => 'Parameter {field} tidak boleh kosong !',
      'numeric' => 'Parameter {field}  hanya boleh diisi angka !',
      'in_list' => 'Parameter {field} hanya boleh diisi => 1 :(Checkin), 2 (Checkout !'
    ]);
    $this->form_validation->set_rules('periode_start', 'Periode Start', 'required|callback_valid_date', [
      'required' => 'Parameter {field} tidak boleh kosong !',
      'valid_date' => 'Format tanggal {field} tidak valid !'
    ]);
    $this->form_validation->set_rules('periode_end', 'Periode End', 'required|callback_valid_date', [
      'required' => 'Parameter {field} tidak boleh kosong !',
      'valid_date' => 'Format tanggal {field} tidak valid !'
    ]);
    $this->form_validation->set_rules('id_reg', 'ID Reg', 'numeric', [
      'numeric' => 'Parameter {field}  hanya boleh diisi angka !',
    ]);
    $this->form_validation->set_rules('is_reg_del_date_null', 'Del Date', 'required|in_list[1,2]', [
      'required' => 'Parameter {field} tidak boleh kosong !',
      'in_list' => 'Parameter {field} hanya boleh diisi => 1 : Yes (NULL), 2 : No (NOT NULL) !'
    ]);
    $this->form_validation->set_rules('jenis_visit', 'Jenis Visit', 'in_list[RI,RJ]', [
      'in_list' => 'Parameter {field} hanya boleh diisi => RI (rawat inap), RJ (rawat jalan)) !'
    ]);
    $this->form_validation->set_rules('no_rm', 'No RM', 'numeric', [
      'numeric' => 'Parameter {field}  hanya boleh diisi angka !',
    ]);
    $this->form_validation->set_rules('is_pasien_baru', 'Is Pasien Baru', 'numeric|in_list[1,2]', [
      'in_list' => 'Parameter {field} hanya boleh diisi => 1 (Pasien Lama), 2 (Pasien Baru) !'
    ]);
    $this->form_validation->set_rules('gender', 'Gender', 'numeric|in_list[1,2]', [
      'in_list' => 'Parameter {field} hanya boleh diisi => 1 (Laki-laki), 2 (Perempuan) !'
    ]);
    $this->form_validation->set_rules('id_dept_ruang_rawat', 'ID Dept Ruang rawat', 'numeric', [
      'numeric' => 'Parameter {field}  hanya boleh diisi angka !',
    ]);
    $this->form_validation->set_rules('id_ref_payment', 'ID Ref payment', 'numeric', [
      'numeric' => 'Parameter {field}  hanya boleh diisi angka !',
    ]);
    $this->form_validation->set_rules('id_dpjp', 'Id Dpjp', 'numeric', [
      'numeric' => 'Parameter {field}  hanya boleh diisi angka !',
    ]);
    $this->form_validation->set_rules('id_kec', 'ID Kec', 'numeric', [
      'numeric' => 'Parameter {field}  hanya boleh diisi angka !',
    ]);
    $this->form_validation->set_rules('id_kel', 'ID Kel', 'numeric', [
      'numeric' => 'Parameter {field}  hanya boleh diisi angka !',
    ]);
    $this->form_validation->set_rules('shift', 'Shift', 'numeric|in_list[1,2,3]', [
      'numeric' => 'Parameter {field}  hanya boleh diisi angka !',
      'in_list' => 'Parameter {field} hanya boleh diisi => 1 , 2 , 3 !'
    ]);
    $this->form_validation->set_rules('offset', 'Offset', 'numeric', [
      'numeric' => 'Parameter {field}  hanya boleh diisi angka !',
    ]);
    $this->form_validation->set_rules('limit', 'Limit', 'greater_than_equal_to[1]|less_than_equal_to[200]|numeric', [
      'numeric' => 'Parameter {field}  hanya boleh diisi angka !',
      'greater_than_equal_to' => 'Parameter {field}  minimal 1 !',
      'less_than_equal_to' => 'Parameter {field}  maksimal 200 !'
    ]);

    /*
    Jika semua parameter yang dikirim sudah sesuai dengan rules yang diset,
    maka request akan diproses. Jika ada parameter yang tidak sesuai, akan diresponse
    dengan status 500 dan error message. Tidak ada yg perlu diubah untuk response 500 
    dan error message nya. 
    */
    if ($this->form_validation->run() == true) {
      /*
      Request data ke model dengan $filter yang dikirim user.
      */
      $dataPasienRegistrasi = $this->model->get($filter);

      if (empty($dataPasienRegistrasi)) {
        /*
        Ini adalah response kalau data yang diterima dari model kosong
        atau NULL.
        Tidak ada yang perlu diubah disini. Untuk status HARUS 200.
        */
        $total = [
          'total_data' => 0,
          'total_filtered' => 0
        ];
        $res['status'] = 200;
        $res['message'] = 'Data Not Found';
        $res['total_data'] = $total;
        $res['data'] = [];
        $this->response($res, $res['status']);
      }

      /* Proses mapping untuk output dari endpoint ini */
      $dataResponse = [];

      foreach ($dataPasienRegistrasi as $key => $val) {
        /*
        Ini filter untuk parameter 'umur'. 
        Tidak ada yang perlu diubah. Silahkan digunakan kl diperlukan, 
        silahkan dihapus jika tidak digunakan.
        */
        $checkinDate = date('Y-m-d', strtotime($val['tanggal_checkin']));
        $diff = date_diff(date_create($val['tanggal_lahir']), date_create($checkinDate));

        if (isset($filter['umur_start']) && !empty($filter['umur_start']) && (!isset($filter['umur_end']) || empty($filter['umur_end']))) {
          $umur = explode(" ", $filter['umur_start']);
          if ($umur[0] != $diff->format('%y') || $umur[1] != $diff->format('%m') || $umur[2] != $diff->format('%d')) {
            continue;
          }
        }

        if (isset($filter['umur_start']) && $filter['umur_start'] != null && isset($filter['umur_end']) && $filter['umur_end'] != null) {
          $umurStart = explode(" ", $filter['umur_start']);
          $umurEnd = explode(" ", $filter['umur_end']);

          $tgl_lahir = strtotime($val['tanggal_lahir']);

          $dateStart = strtotime('+' . $umurStart[0] . ' years', $tgl_lahir);
          $dateStart = strtotime('+' . $umurStart[1] . ' months', $dateStart);
          $dateStart = strtotime('+' . $umurStart[2] . ' days', $dateStart);
          $dateStart = date('Y-m-d H:i:s', $dateStart);

          $dateEnd = strtotime('+' . $umurEnd[0] . ' years', $tgl_lahir);
          $dateEnd = strtotime('+' . $umurEnd[1] . ' months', $dateEnd);
          $dateEnd = strtotime('+' . $umurEnd[2] . ' days', $dateEnd);
          $dateEnd = date('Y-m-d H:i:s', $dateEnd);

          $checkinDate = date('Y-m-d 00:00:00', strtotime($val['tanggal_checkin']));
          if ($checkinDate <= $dateStart || $checkinDate >= $dateEnd) {
            continue;
          }
        }
        /* End of parameter 'umur' */

        /*
        Ini filter untuk parameter 'shift'
        Tidak ada yang perlu diubah. Silahkan digunakan kl diperlukan, 
        silahkan dihapus jika tidak digunakan.       
         */
        $dataShift = null;
        if (isset($filter['shift']) && !empty($filter['shift'])) {
          $dataShift = $this->model->getDataShiftById($filter['shift']);
        }
        if ($filter['jenis_periode'] == 1) {
          $time = date('H:i:s', strtotime($val['tanggal_checkin']));
        } elseif ($filter['jenis_periode'] == 2) {
          $time = date('H:i:s', strtotime($val['tanggal_checkout']));
        }
        if ($dataShift != null) {
          $shift_start = $dataShift['time_start'];
          $shift_end = $dataShift['time_end'];
        } else {
          $shift_start = '00:00:00';
          $shift_end = '23:59:59';
        }
        if ($shift_start < $shift_end) {
          if ($time <= $shift_start || $time >= $shift_end) {
            continue;
          }
        }
        if ($shift_start > $shift_end) {
          if ($time <= $shift_start && $time >= $shift_end) {
            continue;
          }
        }
        /* End of parameter 'shift' */

        /*
        Mapping output endpoint.
        Silahkan disesuaikan dengan tugas masing-masing. Dibawah ini adalah
        contoh format output.
        */
        $dataResponse[] = [
          'data_registrasi' => [
            'id_reg'                 => $val['id_reg'],
            'no_reg'                 => $val['no_reg'],
            'keterangan_pulang'      => $val['level'] >= 1 ? $val['alasan_pulang'] : '',
            'kondisi_pulang'         => $val['kondisi_pulang'],
            'alasan_pulang'          => $val['alasan_pulang'],
            'asal_pasien'            => $val['asal_pasien'],
            'id_dpjp'                => $val['id_dpjp'],
            'nama_dpjp'              => $val['nama_dpjp'],
            'tgl_checkin'            => $val['tanggal_checkin'],
            'nama_petugas_checkin'   => $val['nama_petugas_checkin'],
            'tgl_checkout'           => $val['tanggal_checkout'],
            'nama_petugas_checkout'  => $val['nama_petugas_checkout'],
            'durasi'                 => $val['tanggal_checkout'] != null ? ceil((strtotime($val['tanggal_checkout']) - strtotime($val['tanggal_checkin'])) / 3600) . ' Jam' : '-',
            'lama_rawat'             => $val['lama_rawat'],
            'id_departement'         => $val['id_departement'],
            'jenis_rawat'            => $val['jenis_rawat'],
            'ruang_rawat'            => $val['ruang_rawat'],
            'pemeriksaan_fisik'      => $val['pemeriksaan_fisik'],
            'pemeriksaan_penunjang'  => $val['pemeriksaan_penunjang'],
            'diagnosa_dokter'        => $val['diagnosa_dokter'],
            'terapi_tindakan'        => $val['terapi_tindakan']
          ],
          'data_catatan' => [
            'icd_10_primary' => [
              $val['code_icd10_primary'] => $val['nama_icd10_primary']
            ],
            'icd_10_secondary' => [
              'code' => $val['code_icd10_secondary'],
              'nama' => $val['nama_icd10_secondary']
            ],
            'icd_10_awal' => [
              $val['code_icd10_awal'] => $val['nama_icd10_awal']
            ],
            'icd_9' => [
              'code' => $val['code_icd9'],
              'nama' => $val['nama_icd9']
            ]
          ],
          'data_pasien' => [
            'no_rm'        => $val['no_rm'],
            'no_bpjs'      => $val['no_bpjs'],
            'nama_pasien'  => $val['nama_pasien'],
            'kelamin'      => $val['kelamin'] == "1" ? 'Laki - laki' : 'Perempuan',
            'id_penjamin'  => $val['id_penjamin'],
            'penjamin'     => $val['payment'],
            'dob'          => $val['tanggal_lahir'],
            'umur'         => $val['tanggal_checkin'] < $val['tanggal_lahir'] ? 'FIX DATA' : $diff->format('%y Tahun %m Bulan %d Hari'),
            'alamat_1'     => $val['alamat1'],
            'alamat_2'     => $val['nama_kelurahan'] . ', ' . $val['nama_kecamatan'] . ', ' . $val['nama_kabupaten'] . ', ' . $val['nama_provinsi'],
            'id_kel'       => $val['id_kelurahan'],
            'nama_kel'     => $val['nama_kelurahan'],
            'id_kec'       => $val['id_kecamatan'],
            'nama_kec'     => $val['nama_kecamatan'],
            'status'       => $val['is_pasien_baru'] == "1" ? 'Baru' : 'Lama',
            'kunjungan_ke' => $val['total_kunjungan']
          ]
        ];
      }

      $total_data = count($dataResponse);
      /*
      Ini filter untuk datatables. Tidak perlu diubah.
      JANGAN DIHAPUS
      */
      if (isset($filter['offset']) && isset($filter['limit'])) {
        $dataResponse = array_slice($dataResponse, $filter['offset'], $filter['limit']);
      }

      $total_filtered = count($dataResponse);

      /*
      Jika data hasil mapping kosong atau NULL, maka akan direspon error.
      Tidak perlu ada perubahan disini
      */
      if (empty($dataPasienRegistrasi) || empty($dataResponse)) {
        $total = [
          'total_data' => 0,
          'total_filtered' => 0
        ];
        $res['status'] = 200;
        $res['message'] = 'Data Not Found';
        $res['total_data'] = $total;
        $res['data'] = [];
        $this->response($res, $res['status']);
      }
      /*
      Output dari endpoint ini. Tidak perlu ada yg diubah disini kecuali
      penyesuaian nama variable $dataResponse.
      */
      $total = [
        'total_data' => $total_data,
        'total_filtered' => $total_filtered
      ];

      return $this->response([
        'code' => 200,
        'message' => 'Success',
        'total_data' => $total,
        'data' => $dataResponse
      ], 200);
    }

    $res['status']  = 500;
    $errors = $this->form_validation->error_array();
    $res['message'] = $errors;
    $this->response($res, $res['status']);
  }

  /*
  Endpoint laporan_temp/statistik_registrasi
  $route['laporan_temp/statistik_registrasi'] = 'laporan/c_laporan_temp/statistik_registrasi'; 
  */
  public function statistik_registrasi_get()
  {

    $filter = $this->get();
    $this->load->library('form_validation');

    /*
    Cek apakah parameter yang dikirim ke endpoint diperbolehkan
    atau tidak. Jika parameter yang dikirim tidak ada di list param1,
    maka akan direspon error 500. Tidak ada yang perlu diubah disini.
    */
    foreach ($filter as $key => $val) {
      if (!in_array($key, $this->param2)) {
        $res['status'] = 500;
        $res['message'] = "Error ! Parameter '" .  $key . "' tidak diperbolehkan !!!";
        $this->response($res, $res['status']);
      }
    }

    /*
    Validasi untuk masing-masing parameter yang digunakan di endpoint ini.
    Tolong untuk masing-masing rules, diset error message nya. Beberapa contoh error message
    dapat dilihat dari contoh dibawah ini. Ini adalah form validation standard dari CodeIgniter.
    */
    $this->form_validation->set_data($filter);

    $this->form_validation->set_rules('jenis_periode', 'Jenis Periode', 'required|numeric|in_list[1,2]', [
      'required' => 'Parameter {field} tidak boleh kosong !',
      'numeric' => 'Parameter {field}  hanya boleh diisi angka !',
      'in_list' => 'Parameter {field} hanya boleh diisi => 1 :(Checkin), 2 (Checkout) !'
    ]);
    $this->form_validation->set_rules('periode_start', 'Periode Start', 'required|callback_valid_date', [
      'required' => 'Parameter {field} tidak boleh kosong !',
      'valid_date' => 'Format tanggal {field} tidak valid !'
    ]);
    $this->form_validation->set_rules('periode_end', 'Periode End', 'required|callback_valid_date', [
      'required' => 'Parameter {field} tidak boleh kosong !',
      'valid_date' => 'Format tanggal {field} tidak valid !'
    ]);
    $this->form_validation->set_rules('is_pasien_baru', 'Is Pasien Baru', 'numeric|in_list[1,2]', [
      'in_list' => 'Parameter {field} hanya boleh diisi => 1 (Pasien Lama), 2 (Pasien Baru) !'
    ]);
    $this->form_validation->set_rules('gender', 'Gender', 'numeric|in_list[1,2]', [
      'in_list' => 'Parameter {field} hanya boleh diisi => 1 (Laki-laki), 2 (Perempuan) !'
    ]);
    $this->form_validation->set_rules('id_ref_payment', 'ID Ref payment', 'numeric', [
      'numeric' => 'Parameter {field}  hanya boleh diisi angka !',
    ]);
    $this->form_validation->set_rules('id_dpjp', 'Id Dpjp', 'numeric', [
      'numeric' => 'Parameter {field}  hanya boleh diisi angka !',
    ]);
    $this->form_validation->set_rules('id_kec', 'ID Kec', 'numeric', [
      'numeric' => 'Parameter {field}  hanya boleh diisi angka !',
    ]);
    $this->form_validation->set_rules('id_kel', 'ID Kel', 'numeric', [
      'numeric' => 'Parameter {field}  hanya boleh diisi angka !',
    ]);
    $this->form_validation->set_rules('id_dept_ruang_rawat', 'Depo', 'numeric', [
      'numeric' => 'Parameter {field}  hanya boleh diisi angka !',
    ]);
    $this->form_validation->set_rules('jenis_rawat', 'Jenis Visit', 'in_list[RI,RJ]', [
      'in_list' => 'Parameter {field} hanya boleh diisi => RI (rawat inap), RJ (rawat jalan)) !'
    ]);
    $this->form_validation->set_rules('is_reg_del_date_null', 'Del Date', 'required|in_list[1,2]', [
      'required' => 'Parameter {field} tidak boleh kosong !',
      'in_list' => 'Parameter {field} hanya boleh diisi => 1 : Yes (NULL), 2 : No (NOT NULL) !'
    ]);

    /*
    Jika semua parameter yang dikirim sudah sesuai dengan rules yang diset,
    maka request akan diproses. Jika ada parameter yang tidak sesuai, akan diresponse
    dengan status 500 dan error message. Tidak ada yg perlu diubah untuk response 500 
    dan error message nya. 
    */
    if ($this->form_validation->run() == true) {
      /*
      Request data ke model dengan $filter yang dikirim user.
      */
      $statistik = $this->model->getDataStatistik($filter);

      /*
      Proses mapping hasil request ke model untuk dijadikan output
      */
      $total = 0;
      foreach ($statistik as $k => $v) {
        $kelamin = $v['kelamin'];

        /* 
        Statistik umur
        */
        $checkinDate = date('Y-m-d H:i:s', strtotime($v['checkin_time']));
        $diff = date_diff(date_create($v['tanggal_lahir']), date_create($checkinDate));
        $days = $diff->days;
        $this->getStatistikUmur($days, $kelamin);

        /* 
        Statistik jenis kelamin
        */
        if ($kelamin == 1) $this->dataGender['pria']++;
        if ($kelamin == 2) $this->dataGender['wanita']++;

        /* 
        Statistik status pasien
        */
        if ($v['status_pasien'] == 1) $this->statusPasien['lama']++;
        if ($v['status_pasien'] == 2) $this->statusPasien['baru']++;

        /* 
        Statistik kecamatan, kelurahan, penjamin, dokter, dept ruang rawat.
        Untuk private function nya silahkan disesuaikan dengan tugas masing-masing/
        */
        $this->getStatistikKecamatan($v['id_kecamatan'], $v['nama_kecamatan'], $kelamin);
        $this->getStatistikKelurahan($v['id_kelurahan'], $v['nama_kelurahan'], $kelamin);
        $this->getStatistikPenjamin($v['id_penjamin'], $v['penjamin']);
        $this->getStatistikDokter($v['id_dokter'], $v['nama_dokter']);
        $this->getStatistikDeptRuangRawat($v['id_dept_ruang_rawat'], $v['nama_dept_ruang_rawat']);

        /* Total data */
        $total++;
      }

      $dataResponse['data_kecamatan']     = $this->dataKecamatan;
      $dataResponse['data_kelurahan']     = $this->dataKelurahan;
      $dataResponse['data_kelamin']       = $this->dataGender;
      $dataResponse['data_status_pasien'] = $this->statusPasien;
      $dataResponse['data_penjamin']      = $this->dataPenjamin;
      $dataResponse['data_dokter']        = $this->dataDokter;
      $dataResponse['data_ruang_rawat']   = $this->dataDeptRuangRawat;
      $dataResponse['data_umur']          = [
        '0 Hari - 6 hari'           => $this->arrDays6,
        '6 Hari - 28 hari'          => $this->arrDays28,
        '28 Hari - 1 Thn'           => $this->arrDays360,
        '1 Thn - 4 Thn 11 Bulan'    => $this->arrYear4,
        '5 Thn - 14 Thn 11 Bulan'   => $this->arrYear14,
        '15 Thn - 24 Thn 11 Bulan'  => $this->arrYear24,
        '25 Thn - 44 Thn 11 Bulan'  => $this->arrYear44,
        '45 Thn - 64 Thn 11 Bulan'  => $this->arrYear64,
        '> 65 Thn' => $this->arrYear65
      ];

      /*
      Output dari endpoint ini. Tidak perlu ada yg diubah disini kecuali
      penyesuaian nama variable $dataResponse.
      */
      $res['status'] = 200;
      $res['jumlah_data'] = $total;
      $res['data'] = $dataResponse;
      $this->response($res, $res['status']);
    }

    $res['status']  = 500;
    $errors = $this->form_validation->error_array();
    $res['message'] = $errors;
    $this->response($res, $res['status']);
  }

  /*
  Private function untuk memproses data dari model. Silahkan disesuaikan dengan tugas masing
  masing. Silahkan hapus jika tidak diperlukan.
  */
  private function getStatistikKecamatan($id_kecamatan, $nama_kecamatan, $kelamin)
  {
    if (!isset($this->dataKecamatan[$id_kecamatan])) {
      $this->dataKecamatan[$id_kecamatan] = [
        'nama' => $nama_kecamatan,
        'pria' => 0,
        'wanita' => 0,
      ];

      if ($kelamin == 1) $this->dataKecamatan[$id_kecamatan]['pria']++;
      if ($kelamin == 2) $this->dataKecamatan[$id_kecamatan]['wanita']++;
    } else {
      if ($kelamin == 1) $this->dataKecamatan[$id_kecamatan]['pria']++;
      if ($kelamin == 2) $this->dataKecamatan[$id_kecamatan]['wanita']++;
    }
  }

  private function getStatistikKelurahan($id_kelurahan, $nama_kelurahan, $kelamin)
  {
    if (!isset($this->dataKelurahan[$id_kelurahan])) {
      $this->dataKelurahan[$id_kelurahan] = [
        'nama' => $nama_kelurahan,
        'pria' => 0,
        'wanita' => 0,
      ];

      if ($kelamin == 1) $this->dataKelurahan[$id_kelurahan]['pria']++;
      if ($kelamin == 2) $this->dataKelurahan[$id_kelurahan]['wanita']++;
    } else {
      if ($kelamin == 1) $this->dataKelurahan[$id_kelurahan]['pria']++;
      if ($kelamin == 2) $this->dataKelurahan[$id_kelurahan]['wanita']++;
    }
  }

  private function getStatistikPenjamin($id_penjamin, $nama_penjamin)
  {
    if (!isset($this->dataPenjamin[$id_penjamin])) {
      $this->dataPenjamin[$id_penjamin] =  [
        'nama' => $nama_penjamin,
        'total' => 1
      ];
    } else {
      $this->dataPenjamin[$id_penjamin]['total']++;
    }
  }

  private function getStatistikDokter($id_dokter, $nama_dokter)
  {
    if (!isset($this->dataDokter[$id_dokter])) {
      $this->dataDokter[$id_dokter] = [
        'nama' => $nama_dokter,
        'total' => 1
      ];
    } else {
      $this->dataDokter[$id_dokter]['total']++;
    }
  }

  private function getStatistikDeptRuangRawat($id_dept_ruang_rawat, $nama_dept_ruang_rawat)
  {
    if (!isset($this->dataDeptRuangRawat[$id_dept_ruang_rawat])) {
      $this->dataDeptRuangRawat[$id_dept_ruang_rawat] = [
        'nama' => $nama_dept_ruang_rawat,
        'total' => 1
      ];
    } else {
      $this->dataDeptRuangRawat[$id_dept_ruang_rawat]['total']++;
    }
  }

  private function getStatistikUmur($days, $kelamin)
  {
    $month = 30; // 1 bulan 30 hari
    $year = 12 * 30; // 1 tahun 12 x 30 hari = 360

    if ($days >= 0 && $days <= 6) { // 0 - 6 hari
      if ($kelamin == 1) $this->arrDays6['pria']++;
      if ($kelamin == 2) $this->arrDays6['wanita']++;
    } else if ($days >= 7 && $days <= 28) { // 7 - 28 hari
      if ($kelamin == 1) $this->arrDays28['pria']++;
      if ($kelamin == 2) $this->arrDays28['wanita']++;
    } else if ($days >= 28 && $days <= $year) { // 28 -360 hari
      if ($kelamin == 1) $this->arrDays360['pria']++;
      if ($kelamin == 2) $this->arrDays360['wanita']++;
    } else if ($days > $year && $days <= ($year * 4) + ($month * 11)) { // 1 tahun - 4 tahun 11 bulan
      if ($kelamin == 1) $this->arrYear4['pria']++;
      if ($kelamin == 2) $this->arrYear4['wanita']++;
    } else if ($days > ($year * 5) && $days <= ($year * 14) + ($month * 11)) { // 5 tahun - 14 tahun 11 bulan
      if ($kelamin == 1) $this->arrYear14['pria']++;
      if ($kelamin == 2) $this->arrYear14['wanita']++;
    } else if ($days > ($year * 15) && $days <= ($year * 24) + ($month * 11)) { // 15 tahun - 24 tahun 11 bulan
      if ($kelamin == 1) $this->arrYear24['pria']++;
      if ($kelamin == 2) $this->arrYear24['wanita']++;
    } else if ($days > ($year * 25) && $days <= ($year * 44) + ($month * 11)) { // 25 tahun - 44 tahun 11 bulan
      if ($kelamin == 1) $this->arrYear44['pria']++;
      if ($kelamin == 2) $this->arrYear44['wanita']++;
    } else if ($days > ($year * 45) && $days <= ($year * 64) + ($month * 11)) { // 45 tahun - 64 tahun 11 bulan
      if ($kelamin == 1) $this->arrYear64['pria']++;
      if ($kelamin == 2) $this->arrYear64['wanita']++;
    } else if ($days >= ($year * 65)) { // 65 tahun ke atas
      if ($kelamin == 1) $this->arrYear65['pria']++;
      if ($kelamin == 2) $this->arrYear65['wanita']++;
    }
  }
}
