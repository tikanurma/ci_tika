<?php defined('BASEPATH') or exit('No direct script access allowed');

class C_ref_users_occupation extends MY_Controller
{
	/**
	 * Constructor
	 *
	 * @access public
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();

		$this->load->model('ref/model31/M_ref_users_occupation', 'model');
	}

	public function occupation_get()
	// $route['ref_occ/(:any)'] = 'ref/ref_users_occupation/C_ref_users_occupation/$1';
        {
            $id = $this->get('id');
            if($id==null){
                $occupation =  $this->model->occupation_get();
                $res["status"]	= "200";
                $res["message"] = "Berhasil mendapatkan data Ref Form Header.";
                $res["data"]	  = $occupation;
    
                $this->response($res, 200);
            }
        
            else{
    
                $occupation = $this->model->occupation_get($id);
                $res['status'] = 200;
                $res['message'] = 'Data Form Id';
                $res["data"]	  = $occupation;
    
                $this->response($res, 200);
            }   
        }

	
	public function occupationcreate_post()
	//$route['ref_occ/(:any)'] = 'ref/ref_users_occupation/C_ref_users_occupation/$1';
    {   
        $data = [
            'id' => $this->post('id'),
            'occupation' => $this->post('occupation')
        ];

        if ($this->model->createOccupation($data) > 0) {
            $res["status"]	= "200";
			$res["message"] = "Berhasil dikirim";
			$this->response($res, 200);
		 
        } else {
            $res['status'] = 400;
            $res['message'] = 'Gagal upload data';
			$this->response($res, 200);

        }
        
	}
	
	function updateoccupation_put()
	//$route['ref_occ/(:any)'] = 'ref/ref_users_occupation/C_ref_users_occupation/$1';
    {
        $id=$this->put('id');
		$data = [
            'id' => $this->put('id'),
            'occupation' => $this->put('occupation')
        ];

        if ($this->model->updateOccupation($data,$id) > 0) {
			$res["status"]	= "200";
			$res["message"] = "Berhasil diupdate";
			$this->response($res, 200);
		} 
		
		else {
            $res['status'] = 400;
            $res['message'] = 'Gagal update data';
			$this->response($res, 200);
        }
    }
}
