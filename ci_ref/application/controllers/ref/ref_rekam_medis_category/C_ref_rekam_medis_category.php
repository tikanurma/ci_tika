<?php defined('BASEPATH') or exit('No direct script access allowed');

class C_ref_rekam_medis_category extends MY_Controller
{
	/**
	 * Constructor
	 *
	 * @access public
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();

		$this->load->model('ref/model31/M_ref_rekam_medis_category', 'model');
	}

	public function rekam_get()
	// $route['ref_rekam/(:any)'] = 'ref/ref_rekam_medis_category/C_ref_rekam_medis_category/$1';
    
    {
        $id = $this->get('id');
        if($id==null){
            $rekam =  $this->model->rekammedis_get();
            $res["status"]	= "200";
		    $res["message"] = "Berhasil mendapatkan data Ref Form Header.";
		    $res["data"]	  = $rekam;

		    $this->response($res, 200);
        }
    
        else{

            $rekam = $this->model->rekammedis_get($id);
            $res['status'] = 200;
            $res['message'] = 'Data Form Id';
            $res["data"]	  = $rekam;


			$this->response($res, 200);

        }
		
	}
	
	public function rekamcreate_post()
	//$route['ref_rekam/(:any)'] = 'ref/ref_rekam_medis_category/C_ref_rekam_medis_category/$1';
    {   
        $data = [
            'id' => $this->post('id'),
            'rekam_medis_category' => $this->post('rekam_medis_category')
        ];

        if ($this->model->createRekamMedis($data) > 0) {
            $res["status"]	= "200";
			$res["message"] = "Berhasil dikirim";
			$this->response($res, 200);
		 
        } else {
            $res['status'] = 400;
            $res['message'] = 'Gagal upload data';
			$this->response($res, 200);

        }
        
	}
	
	function updaterekam_put()
	//$route['ref_rekam/(:any)'] = 'ref/ref_rekam_medis_category/C_ref_rekam_medis_category/$1';
    {
        $id=$this->put('id');
		$data = [
            'id' => $this->put('id'),
            'rekam_medis_category' => $this->put('rekam_medis_category')
        ];

        if ($this->model->updateRekamMedis($data,$id) > 0) {
			$res["status"]	= "200";
			$res["message"] = "Berhasil diupdate";
			$this->response($res, 200);
		} 
		
		else {
            $res['status'] = 400;
            $res['message'] = 'Gagal update data';
			$this->response($res, 200);
        }
    }
}
