<?php defined('BASEPATH') or exit('No direct script access allowed');

class C_ref_users_pendidikan extends MY_Controller
{
	/**
	 * Constructor
	 *
	 * @access public
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();

		$this->load->model('ref/model31/M_ref_users_pendidikan', 'model');
	}

	public function pendidikan_get()
	// $route['ref_pendidikan/(:any)'] = 'ref/ref_users_pendidikan/C_ref_users_pendidikan/$1';
    
    {
        $id = $this->get('id');
        if($id==null){
            $pendidikan=  $this->model->pendidikan_get();
            $res["status"]	= "200";
		    $res["message"] = "Berhasil mendapatkan data Ref Form Header.";
		    $res["data"]	  = $pendidikan;

		    $this->response($res, 200);
        }
    
        else{

            $pendidikan = $this->model->pendidikan_get($id);
            $res['status'] = 200;
            $res['message'] = 'Data Form Id';
            $res["data"]	  = $pendidikan;


			$this->response($res, 200);

        }
		
	}
	
	public function pendidikancreate_post()
	//$route['ref_pendidikan/(:any)'] = 'ref/ref_users_pendidikan/C_ref_users_pendidikan/$1';
    {   
        $data = [
            'id' => $this->post('id'),
            'pendidikan' => $this->post('pendidikan')
        ];

        if ($this->model->createPendidikan($data) > 0) {
            $res["status"]	= "200";
			$res["message"] = "Berhasil dikirim";
			$this->response($res, 200);
		 
        } else {
            $res['status'] = 400;
            $res['message'] = 'Gagal upload data';
			$this->response($res, 200);

        }
        
	}
	
	function updatependidikan_put()
	//$route['ref_pendidikan/(:any)'] = 'ref/ref_users_pendidikan/C_ref_users_pendidikan/$1';
    {
        $id=$this->put('id');
		$data = [
            'id' => $this->put('id'),
            'pendidikan' => $this->put('pendidikan')
        ];

        if ($this->model->updatePendidikan($data,$id) > 0) {
			$res["status"]	= "200";
			$res["message"] = "Berhasil diupdate";
			$this->response($res, 200);
		} 
		
		else {
            $res['status'] = 400;
            $res['message'] = 'Gagal update data';
			$this->response($res, 200);
        }
    }
}
