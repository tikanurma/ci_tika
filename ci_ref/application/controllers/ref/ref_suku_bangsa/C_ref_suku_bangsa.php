<?php defined('BASEPATH') or exit('No direct script access allowed');

class C_ref_suku_bangsa extends MY_Controller
{
	/**
	 * Constructor
	 *
	 * @access public
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();

		$this->load->model('ref/model31/M_suku_bangsa', 'model');
	}

	public function suku_get()
	// $route['ref_suku/(:any)'] = 'ref/ref_suku_bangsa/c_ref_suku_bangsa/$1';
    
    {
        $id = $this->get('id');
        if($id==null){
            $suku1 =  $this->model->suku_bangsa_get();
            $res["status"]	= "200";
		    $res["message"] = "Berhasil mendapatkan data Ref Form Header.";
		    $res["data"]	  = $suku1;

		    $this->response($res, 200);
        }
    
        else{

            $suku2 = $this->model->suku_bangsa_get($id);
            $res['status'] = 200;
            $res['message'] = 'Data Form Id';
            $res["data"]	  = $suku2;


			$this->response($res, 200);

        }
		
	}
	
	public function sukucreate_post()
	//$route['ref_suku/(:any)'] = 'ref/ref_suku_bangsa/c_ref_suku_bangsa/$1';
    {   
        $data = [
            'id' => $this->post('id'),
            'nama_suku_bangsa' => $this->post('nama_suku_bangsa')
        ];

        if ($this->model->createSuku($data) > 0) {
            $res["status"]	= "200";
			$res["message"] = "Berhasil dikirim";
			$this->response($res, 200);
		 
        } else {
            $res['status'] = 400;
            $res['message'] = 'Gagal upload data';
			$this->response($res, 200);

        }
        
	}
	
	function updatesuku_put()
	//$route['ref_suku/(:any)'] = 'ref/ref_suku_bangsa/c_ref_suku_bangsa/$1';
    {
        $id=$this->put('id');
		$data = [
            'id' => $this->put('id'),
            'nama_suku_bangsa' => $this->put('nama_suku_bangsa')
        ];

        if ($this->model->updateSuku($data,$id) > 0) {
			$res["status"]	= "200";
			$res["message"] = "Berhasil diupdate";
			$this->response($res, 200);
		} 
		
		else {
            $res['status'] = 400;
            $res['message'] = 'Gagal update data';
			$this->response($res, 200);
        }
    }
}
