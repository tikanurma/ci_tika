<?php defined('BASEPATH') or exit('No direct script access allowed');

class C_ref_users_emergency_contact_relationship_to_patient extends MY_Controller
{
	/**
	 * Constructor
	 *
	 * @access public
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();

		$this->load->model('ref/model31/M_ref_users_emergency_contact_relationship_to_patient', 'model');
	}

	public function relationship_get()
	// $route['ref_relationship/(:any)'] = 'ref/ref_users_emergency_contact_relationship_to_patient/C_ref_users_emergency_contact_relationship_to_patient/$1';
    
    {
        $id = $this->get('id');
        if($id==null){
            $relationship =  $this->model->relationship_get();
            $res["status"]	= "200";
		    $res["message"] = "Berhasil mendapatkan data Ref Form Header.";
		    $res["data"]	  = $relationship;

		    $this->response($res, 200);
        }
    
        else{

            $relationship = $this->model->relationship_get($id);
            $res['status'] = 200;
            $res['message'] = 'Data Form Id';
            $res["data"]	  = $relationship;


			$this->response($res, 200);

        }
		
	}
	
	public function relationshipcreate_post()
	//$route['ref_relationship/(:any)'] = 'ref/ref_users_emergency_contact_relationship_to_patient/C_ref_users_emergency_contact_relationship_to_patient/$1';;
    {   
        $data = [
            'id' => $this->post('id'),
            'relationship_to_patient' => $this->post('relationship_to_patient')
        ];

        if ($this->model->createRelationship($data) > 0) {
            $res["status"]	= "200";
			$res["message"] = "Berhasil dikirim";
			$this->response($res, 200);
		 
        } else {
            $res['status'] = 400;
            $res['message'] = 'Gagal upload data';
			$this->response($res, 200);

        }
        
	}
	
	function updaterelationship_put()
	//$route['ref_relationship/(:any)'] = 'ref/ref_users_emergency_contact_relationship_to_patient/C_ref_users_emergency_contact_relationship_to_patient/$1';
    {
        $id=$this->put('id');
		$data = [
            'id' => $this->put('id'),
            'relationship_to_patient' => $this->put('relationship_to_patient')
        ];

        if ($this->model->updateRelationship($data,$id) > 0) {
			$res["status"]	= "200";
			$res["message"] = "Berhasil diupdate";
			$this->response($res, 200);
		} 
		
		else {
            $res['status'] = 400;
            $res['message'] = 'Gagal update data';
			$this->response($res, 200);
        }
    }
}
