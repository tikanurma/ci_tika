<?php
defined('BASEPATH') or exit('No direct script access allowed');

class C_ref_users_blood_type extends MY_Controller
{
  function __construct()
  {
      parent::__construct();
      $this->load->model('ref/model31/M_ref_users_blood_type', 'model');
  }

  public function blood_get()
//   $route['ref_blood/(:any)'] = 'ref/ref_users_blood_type/C_ref_users_blood_type/$1';  
{
    $data  = $this->model->blood_get();

    $res['status']  = 200;
    $res['message'] = "Berhasil mendapatkan data tarif";
    $res['data']    = $data;

    $this->response($res, $res['status']);
  }
}