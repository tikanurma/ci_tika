<?php defined('BASEPATH') or exit('No direct script access allowed');

class C_ref_global extends MY_Controller
{
	/**
	 * Constructor
	 *
	 * @access public
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();

		$this->load->model('global/M_ref_global', 'mrg');
	}

	public function form_get()
	// $route['form'] = 'global/c_ref_global/form';
	{
		$data = $this->get();

		if (empty($data['id'])) {
			$res['status'] = 400;
			$res['message'] = 'Mohon masukan ID Form';

			$this->response($res, 200);
		}

		$data = $this->mrg->get_form($data['id']);

		$res["status"]	= "200";
		$res["message"] = "Berhasil mendapatkan data Ref Form Header.";
		$res["data"]	  = $data;

		$this->response($res, 200);
	}
}
