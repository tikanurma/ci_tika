<?php
defined('BASEPATH') or exit('No direct script access allowed');

$route['default_controller'] = 'welcome';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;

/// Routes for form ///
$route['form'] = 'global/c_ref_global/form';
$route['ref_suku/(:any)'] = 'ref/ref_suku_bangsa/c_ref_suku_bangsa/$1';
$route['ref_blood/(:any)'] = 'ref/ref_users_blood_type/C_ref_users_blood_type/$1';
$route['ref_occ/(:any)'] = 'ref/ref_users_occupation/C_ref_users_occupation/$1';
$route['ref_pendidikan/(:any)'] = 'ref/ref_users_pendidikan/C_ref_users_pendidikan/$1';
$route['ref_relationship/(:any)'] = 'ref/ref_users_emergency_contact_relationship_to_patient/C_ref_users_emergency_contact_relationship_to_patient/$1';
$route['ref_rekam/(:any)'] = 'ref/ref_rekam_medis_category/C_ref_rekam_medis_category/$1';
/// ENd of routes for form