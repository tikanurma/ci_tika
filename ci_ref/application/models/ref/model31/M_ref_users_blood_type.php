<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class M_ref_users_blood_type extends CI_Model {

    public function blood_get()
	{
        $this->db->select('
        id id,
        blood blood,
        ');
        $this->db->from('ref_users_blood_type');
    
        $query = $this->db->get()->result_array();
        return $query;
    }
}