<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class M_ref_rekam_medis_category extends CI_Model {


    public function rekammedis_get($id = null) {

        if($id === null){
             $this->db->select(" a.id id, a.rekam_medis_category rekam_medis_category ");
             $this->db->from('ref_rekam_medis_category a');
             $this->db->where('a.is_del', 2);
             $query = $this->db->get()->result_array();
             return $query;
        }
        else {
            return $this->db->get_where('ref_rekam_medis_category', ['id' => $id])->row_array();
      }
    }


    public function createRekamMedis($data){
        $this->db->insert('ref_rekam_medis_category',$data);
        return $this->db->affected_rows();    
    }

    public function updateRekamMedis($data,$id){
        $this->db->update('ref_rekam_medis_category',$data, ['id'=>$id]);
        return $this->db->affected_rows();
        
    }
    
}