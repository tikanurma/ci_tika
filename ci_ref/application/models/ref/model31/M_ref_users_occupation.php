<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class M_ref_users_occupation extends CI_Model {


    public function occupation_get($id = null) {

        if($id === null){
             $this->db->select(" a.id id, a.occupation occupation ");
             $this->db->from('ref_users_occupation a');
             $this->db->where('a.is_del', 2);
             $query = $this->db->get()->result_array();
             return $query;
        }
        else {
            return $this->db->get_where('ref_users_occupation', ['id' => $id])->row_array();
      }
    }


    public function createOccupation($data){
        $this->db->insert('ref_users_occupation',$data);
        return $this->db->affected_rows();    
    }

    public function updateOccupation($data,$id){
        $this->db->update('ref_users_occupation',$data, ['id'=>$id]);
        return $this->db->affected_rows();
        
    }
    
}