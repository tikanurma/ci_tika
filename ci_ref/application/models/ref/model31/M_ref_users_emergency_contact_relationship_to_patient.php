
<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class M_ref_users_emergency_contact_relationship_to_patient extends CI_Model {

    public function relationship_get($id = null)
	{      
        if($id === null){
        $this->db->select(" a.id id, a.relationship_to_patient relationship_to_patient");
        $this->db->from('ref_users_emergency_contact_relationship_to_patient a');
        $this->db->where('a.is_del', 2);
        $query = $this->db->get()->result_array();
        return $query;
    }
   else {
       return $this->db->get_where('ref_users_emergency_contact_relationship_to_patient', ['id' => $id])->row_array();
    }
}

    public function createRelationship($data){
        $this->db->insert('ref_users_emergency_contact_relationship_to_patient',$data);
        return $this->db->affected_rows();    
    }

    public function updateRelationship($data,$id){
        $this->db->update('ref_users_emergency_contact_relationship_to_patient',$data, ['id'=>$id]);
        return $this->db->affected_rows();
        
    }
    
}