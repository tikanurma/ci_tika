<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class M_suku_bangsa extends CI_Model {

    public function suku_bangsa_get($id = null)
	{
        if($id === null){
            $this->db->select(" s.id id, s.nama_suku_bangsa nama_suku_bangsa");
            $this->db->from('ref_suku_bangsa s');
            $this->db->where('s.is_del', 2);
            $query = $this->db->get()->result_array();
            return $query;
        }
        else {
            return $this->db->get_where('ref_suku_bangsa', ['id' => $id])->row_array();
    }
  
}

    public function createSuku($data){
         $this->db->insert('ref_suku_bangsa',$data);
        return $this->db->affected_rows();    
    }

    public function updateSuku($data,$id){
        $this->db->update('ref_suku_bangsa',$data, ['id'=>$id]);
        return $this->db->affected_rows();
        
    }
    
}