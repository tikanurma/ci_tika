<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class M_ref_users_pendidikan extends CI_Model {

    public function pendidikan_get($id = null)
	{      
        if($id === null){
        $this->db->select(" a.id id, a.pendidikan pendidikan");
        $this->db->from('ref_users_pendidikan a');
        $this->db->where('a.is_del', 2);
        $query = $this->db->get()->result_array();
        return $query;
    }
   else {
       return $this->db->get_where('ref_users_pendidikan', ['id' => $id])->row_array();
    }
}

    public function createPendidikan($data){
        $this->db->insert('ref_users_pendidikan',$data);
        return $this->db->affected_rows();    
    }

    public function updatePendidikan($data,$id){
        $this->db->update('ref_users_pendidikan',$data, ['id'=>$id]);
        return $this->db->affected_rows();
        
    }
    
}