<?php defined('BASEPATH') or exit('No direct script access allowed');

class M_ref_global extends CI_Model
{
	
	public function get_form($id)
	{
		$this->db->where("id", $id);
		$sql = $this->db->get('ref_form_header');
		$result = $sql->row_array();

		return $result;
	}
}
