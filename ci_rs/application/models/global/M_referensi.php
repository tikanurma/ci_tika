<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_referensi extends CI_Model {

  public function get_all_kelas_perawatan()
  {
    $this->db->select('
    id id_kelas_perawatan,
    kelas_perawatan kelas_perawatan,
    kode kode_kelas_perawatan
    ');
    $this->db->from('ref_siranap_kode_kelas_perawatan');

    $query = $this->db->get()->result_array();
    return $query;
  }

  public function get_all_ruang_perawatan()
  {
    $this->db->select('
    id id_ruang_perawatan,
    ruang_perawatan ruang_perawatan,
    kode_siranap kode_ruang_perawatan
    ');
    $this->db->from('ref_siranap_kode_ruang_perawatan');

    $query = $this->db->get()->result_array();
    return $query;
  }

  public function get_all_ruang_fasyankes()
  {
    $this->db->select('
    id id_ruangan,
    nama_ruangan nama_ruangan,
    kode_siranap kode_ruangan,
    id_tt id_tempat_tidur
    ');
    $this->db->from('ref_rsonline_fasyankes');

    $query = $this->db->get()->result_array();
    return $query;
  }

  public function get_penjamin()
  {
    $penjamin = $this->db->get_where('ref_payment', ['is_del' => '0'])->result_array();
    // return $penjamin;
    $i = 0;
    foreach ($penjamin as $pk => $pv) {

        if ($pv['sub_payment'] == '0') {
          $data[$i] = [
          'id' => $pv['id_ref_payment'],
          'nama' => $pv['payment'],
          'prefix' => $pv['prefix'],
          'persentase_biaya_adm' => $pv['persentase_biaya_adm']
          ];
        } else if ($pv['sub_payment'] == '1') {
          foreach ($data as $dk => $dv) {
            if ($dv['prefix'] == $pv['prefix']) {
              $data[$dk]['sub_payment'][] = [
              'id' => $pv['id_ref_payment'],
              'nama' => $pv['payment'],
              'prefix' => $pv['prefix'],
              'persentase_diskon' => $pv['persentase_discount']
              ];
            }
          }
        }

      $i++;
    }

    return $data;
  }

  public function get_bt_order_dept($params) {
    $this->db->select("
        a.id id_bt_order,
        a.id_bt_master_dept_job id_bt_master_dept_job,
        b.nama_master_dept_job nama_master_dept_job,
        a.nilai_order_dept nilai_order_dept,

    ");
    $this->db->from('bt_order_dept a');
    $this->db->join('bt_master_dept_job b', 'b.id=a.id_bt_master_dept_job', 'LEFT');
    $this->db->where('a.is_del', 0);
    $this->db->where('b.is_del', 0);
    if(isset($params['id']) && $params['id']) {
      $this->db->where('a.id', $params['id']);
    }
    return $this->db->get()->result_array();
  }

}

/* End of file `M_referensi`.php */
?>
