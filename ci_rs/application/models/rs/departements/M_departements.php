<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_departements extends CI_Model {

    public function get($id=null)
    {
        $this->db->select('departements.departement_id AS departement_id,
                            departements.type AS type,
                            departements.departement_name AS departement_name,
                            departements.rs_key AS rs_key,
                            departements.is_order_dept AS is_order_dept,
                            departements.id_jenis_kegiatan_rajal AS id_jenis_kegiatan_rajal,
                            departements.is_antrean_cs AS is_antrean_cs,
                            departements.is_antrean_status AS is_antrean_status,
                            departements.id_jenis_pelayanan_ranap AS id_jenis_pelayanan_ranap,
                            departements.is_diorder_dipoli AS is_diorder_dipoli,
                            departements.kdpoli AS kdpoli,
                            departements.nmpoli AS nmpoli,
                            departements.prefix AS prefix,
                            departements.start_serving AS start_serving,
                            departements.end_serving AS end_serving,
                            departements.serving_interval AS serving_interval,
                            departements.kuota_antrean_simple AS kuota_antrean_simple,
                            departements.kuota_antrean_app AS kuota_antrean_app,
                            departements.kuota_antrean_mobile_jkn AS kuota_antrean_mobile_jkn,
                            departements.kuota_antrean_anjungan AS kuota_antrean_anjungan,
                            departements_group.nama_type AS nama_type,
                            clinic_profile.rs_key AS cp_rs_key,
                            ref_jenis_kegiatan_rajal.jenis_kegiatan AS jenis_kegiatan_rajal,
                            ref_jenis_pelayanan_ranap.jenis_pelayanan AS jenis_pelayanan_ranap,

                            ');
        $this->db->from('departements');
        $this->db->join('departements_group', 'departements.type = departements_group.id', 'left');
        $this->db->join('clinic_profile', 'departements.rs_key = clinic_profile.rs_key');
        $this->db->join('ref_jenis_kegiatan_rajal', 'departements.id_jenis_kegiatan_rajal = ref_jenis_kegiatan_rajal.id', 'left');
        $this->db->join('ref_jenis_pelayanan_ranap', 'departements.id_jenis_pelayanan_ranap = ref_jenis_pelayanan_ranap.id', 'left');

        if ($id != null)
        {
            $this->db->where('departements.departement_id', $id);
        }
        // $this->db->where('departements.type', '7');
        $this->db->where('departements.is_del', '0');
        $this->db->order_by('departements.departement_id', 'DESC');


        return $this->db->get()->result_array();


    }


    public function add($params)
    {
        $this->db->insert('departements', $params);
        return $this->db->affected_rows();
    }

    public function del($id)
    {
        $this->db->where('departement_id', $id);
        $this->db->set('is_del', '1');
        $this->db->update('departements');
    }

    public function edit($data, $id)
    {
        $this->db->update('departements', $data, ['departement_id' => $id]);
        return $this->db->affected_rows();
    }

}

/* End of file M_departements.php */
