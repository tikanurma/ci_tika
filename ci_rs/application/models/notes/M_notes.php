<?php
defined('BASEPATH') or exit('No direct script access allowed');

class M_notes extends CI_Model
{

    public function get($id = null, $id_ref_global_tipe_42, $no_rm)
    {
        $this->db->select('*');
        $this->db->from('notes');
        if ($id != null) {
            $this->db->where('id_pasien_visit', $id);
        }
        $this->db->where('del_date', null);
        $this->db->where('no_rm', $no_rm);

        $this->db->where('id_ref_global_tipe_42', $id_ref_global_tipe_42);

        $this->db->order_by('notes.id', 'DESC');
        $query = $this->db->get()->result_array();

        return $query;
    }
    
    public function get_by_id_reg($id = null, $id_ref_global_tipe_42, $no_rm)
    {
        $this->db->select('*');
        $this->db->from('notes');
        if ($id != null) {
            $this->db->where('id_pasien_registrasi', $id);
        }
        $this->db->where('del_date', null);
        $this->db->where('no_rm', $no_rm);

        $this->db->where('id_ref_global_tipe_42', $id_ref_global_tipe_42);

        $this->db->order_by('notes.id', 'DESC');
        $query = $this->db->get()->result_array();

        return $query;
    }

    public function get_detail($id = null, $tipe42 = null)
    {
        $this->db->select('*');
        $this->db->from('notes');
        if ($id != null) {
            $this->db->where('id', $id);
        }
        if ($tipe42 != null) {
            $this->db->where('id_ref_global_tipe_42', $tipe42);
        }
        $this->db->where('del_date', null);

        $query = $this->db->get()->result_array();

        return $query;
    }

    public function add($params)
    {
        return $this->db->insert('notes', $params);
    }

    public function edit($id, $data)
    {
        $this->db->where(['id' => $id]);

        $check = $this->db->update('notes', $data);

        if ($check) {
            $result = 1;
        } else {
            $result = 0;
        }

        return $result;
    }

    public function add_vitals($vitals)
    {
        $this->db->insert('notes_vitals', $vitals);
        $insert_id = $this->db->insert_id();
        return $insert_id;
    }

    public function edit_vitals($id, $data)
    {
        $this->db->where(['id' => $id]);

        $check = $this->db->update('notes_vitals', $data);

        if ($check) {
            $result = 1;
        } else {
            $result = 0;
        }

        return $result;
    }

    public function get_vitals($id_notes_vitals)
    {
        $this->db->select("
                a.id id,
                a.id_pasien_registrasi id_pasien_registrasi,
                a.id_pasien_visit id_pasien_visit,
                a.height height,
                a.weight weight,
                a.systolic systolic,
                a.diastolic diastolic,
                a.blood_pressure blood_pressure,
                a.temperature temperature,
                a.pulse pulse,
                a.respiratory_rate respiratory_rate,
                a.kesadaran kesadaran,
                a.keadaan_umum keadaan_umum,
                a.nyeri nyeri,
                a.eye_opening eye_opening,
                a.verbal_response verbal_response,
                a.motor_response motor_response,
                a.spo2 spo2,
                a.akral akral,
                a.reflek_cahaya reflek_cahaya,
                a.pupil_isokor pupil_isokor,
                a.pupil_unisokor pupil_unisokor,
                a.created_date created_date,
                a.created_by created_by
        ");
        $this->db->from('notes_vitals a');
        $this->db->where('id', $id_notes_vitals);
        $this->db->where('del_date', null);
        $this->db->order_by('id', 'ASC');
        return $this->db->get()->result_array();
    }

    public function get_data_grafik($id_reg)
    {
        $this->db->select("
            id id,
            created_date tanggal,
            respiratory_rate respiratory_rate,
            pulse pulse,
            temperature temperature,
            blood_pressure blood_pressure,
            diastolic diastolic,
            systolic systolic
        ");
        $this->db->from('notes_vitals');
        $this->db->where('id_pasien_registrasi', $id_reg);
        $this->db->where('del_date', null);
        $this->db->order_by('tanggal', 'DESC');
        return $this->db->get()->result_array();
    }
}

/* End of file M_notes.php */
