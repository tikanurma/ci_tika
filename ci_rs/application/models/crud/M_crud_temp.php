<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class M_crud_temp extends CI_Model {

  public function get($params)
  {
    $this->db->select("
      a.id id,
      a.payment_prefix prefix_penjamin,
      b.payment nama_penjamin,
      a.id_dept id_dept,
      a.jenis_pasien jenis_pasien,
      c.departement_name nama_dept,
      a.id_bt_order_dept id_tarif_dept
    ");
    $this->db->from("master_auto_tarif a");
    $this->db->join('ref_payment b', 'a.payment_prefix = b.prefix', 'left');
    $this->db->join('departements c', 'c.departement_id = a.id_dept', 'left');
    $this->db->where('a.is_del', '0');
    $this->db->where('b.is_del', '0');

    if (!empty($params['id'])) {
      $this->db->where('a.id', $params['id']);
    }

    if (!empty($params['id_dept'])) {
      $this->db->where('a.id_dept', $params['id_dept']);
    }

    if (!empty($params['prefix_penjamin'])) {
      $this->db->where('a.payment_prefix', $params['prefix_penjamin']);
    }

    if (!empty($params['jenis_pasien'])) {

      $this->db->group_start();
        $this->db->where('a.jenis_pasien', strtoupper($params['jenis_pasien']));
        $this->db->or_where('a.jenis_pasien', 'ALL');
      $this->db->group_end();

    }


    $this->db->group_by('a.id');
    $this->db->order_by('a.id', 'asc');
    $data_auto_tarif = $this->db->get()->result_array();

    foreach ($data_auto_tarif as $k => $v) {
      if (!empty($v['id_tarif_dept'])) {

        $this->db->select("
          a.id id_tarif,
          b.nama_master_dept_job nama_tarif,
          a.nilai_order_dept harga_tarif
        ");
        $this->db->from("bt_order_dept a");
        $this->db->join('bt_master_dept_job b', 'a.id_bt_master_dept_job = b.id');
        $this->db->where('a.is_del', '0');
        $this->db->where('b.is_del', '0');
        $this->db->where('a.id', $v['id_tarif_dept']);

        $data_auto_tarif[$k]['tarif_dept'] = $this->db->get()->result_array();

        foreach ($data_auto_tarif[$k]['tarif_dept'] as $k2 => $v2) {
          $this->db->select("
            c.id id_jasmed,
            c.nama_master_jasmed nama_jasmed,
            a.nilai_order_dept_jasmed tarif_jasmed,
            c.is_single is_single
          ");
          $this->db->from("bt_order_dept_jasmed a");
          $this->db->join('bt_order_dept b', 'a.id_bt_order_dept = b.id');
          $this->db->join('bt_master_jasmed c', 'a.id_bt_master_jasmed = c.id');
          $this->db->where('b.is_del', '0');
          $this->db->where('b.id', $v2['id_tarif']);

          $data_auto_tarif[$k]['tarif_dept'][$k2]['jasmed'] = $this->db->get()->result_array();
        }
      }

    }

    return $data_auto_tarif;
  }

  public function add($params)
  {
    $this->db->insert('master_auto_tarif', $params);
    $id = $this->db->insert_id();
    return $id;
  }

  public function update($params)
  {
      $this->db->where('id', $params['id']);
      return $this->db->update('master_auto_tarif', $params);
  }
}

/* End of file M_auto_tarif.php */

?>
