<?php defined('BASEPATH') OR exit('No direct script access allowed');

use chriskacerguis\RestServer\RestController;
use chriskacerguis\RestServer\Format;

header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Headers: Content-Type, Content-Length, Accept-Encoding, x-token");
header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");

class C_referensi extends MY_Controller
{

  function __construct()
  {
    parent::__construct();
    $this->load->model('global/M_referensi', 'ref');
  }
  // $route['ref/(:any)']    = 'global/c_ref/$1';

  public function ruang_fasyankes_get()
  {

    $get = $this->get();

    $data = $this->ref->get_all_ruang_fasyankes();

    $res['status']  = 200;
    $res['message'] = "Berhasil mendapatkan data.";
    $res['data']    = $data;

    $this->response($res, $res['status']);

  }

  public function kelas_perawatan_get()
  {

    $get = $this->get();

    $data = $this->ref->get_all_kelas_perawatan();

    $res['status']  = 200;
    $res['message'] = "Berhasil mendapatkan data.";
    $res['data']    = $data;

    $this->response($res, $res['status']);
  }

  public function ruang_perawatan_get()
  {

    $get = $this->get();

    $data = $this->ref->get_all_ruang_perawatan();

    $res['status']  = 200;
    $res['message'] = "Berhasil mendapatkan data.";
    $res['data']    = $data;

    $this->response($res, $res['status']);

  }

  public function penjamin_get()
  {
    $get = $this->get();

    $data = $this->ref->get_penjamin($get);

    $res['status']  = 200;
    $res['message'] = "Berhasil mendapatkan data.";
    $res['data']    = $data;

    $this->response($res, $res['status']);
  }

  public function bt_order_dept_get()
  {
    $params = $this->get();
    $data = $this->ref->get_bt_order_dept($params);

    $res['status']  = 200;
    $res['message'] = "Berhasil mendapatkan data.";
    $res['data']    = $data;

    $this->response($res, $res['status']);
  }
}
