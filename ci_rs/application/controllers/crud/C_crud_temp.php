<?php
defined('BASEPATH') or exit('No direct script access allowed');

class C_crud_temp extends MY_Controller
{
  function __construct()
  {
      parent::__construct();
      $this->load->model('crud/M_crud_temp', 'crud_temp');
  }

  public function index_get()
  // $route['crud_temp'] = 'rs/c_crud_temp';
  {
    $get = $this->get();
    $data  = $this->crud_temp->get($get);

    $res['status']  = 200;
    $res['message'] = "Berhasil mendapatkan data tarif";
    $res['data']    = $data;

    $this->response($res, $res['status']);
  }

  public function index_post()
  {
    $params = $this->post();
    $id_master_crud_temp = $this->crud_temp->add($params);

    if ($this->db->affected_rows() > 0) {
      $res['status']  = 200;
      $res['message'] = "Berhasil menambhakan data tarif";
    }
    $this->response($res, $res['status']);

  }

  public function index_put()
  {
      $params = $this->put();
      $data = $this->crud_temp->update($params);

      if ($this->db->affected_rows() > 0) {
          $res['status']  = 200;
          $res['message'] = 'Berhasil update data master auto tarif';
      } else {
          $res['status']  = 200;
          $res['message'] = 'Gagal update data /tidak ada perubahan data master auto tarif';
      }
      $this->response($res, $res['status']);
  }

  public function index_delete()
  {
      $params = $this->delete();
      $data = $this->crud_temp->update($params);

      if ($this->db->affected_rows() > 0) {
          $res['status']  = 200;
          $res['message'] = 'Berhasil hapus data master auto tarif';
      } else {
          $res['status']  = 400;
          $res['message'] = 'Gagal hapus data  master auto tarif';
      }
      $this->response($res, $res['status']);
  }


}
