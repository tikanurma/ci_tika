<?php
defined('BASEPATH') or exit('No direct script access allowed');

$route['default_controller'] = 'welcome';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;


///////// TIDAK PERLU DIUBAH /////////
$route['clinic_profile'] = 'rs/c_clinic_profile';

$route['kunjungan'] = 'rs/c_pasien';
$route['ref/dokter'] = 'rs/c_hrd/dokter';
$route['ref/perawat'] = 'rs/c_hrd/perawat';


// START ROUTES FOR NOTES
$route['notes'] = 'rs/c_notes';
$route['notes_registrasi'] = 'rs/c_notes_registrasi';
$route['notes_vitals_notes'] = 'rs/c_notes/vitals';
$route['notes/detail'] = 'rs/c_notes/detail';
$route['id_notes_vitals'] = 'rs/c_notes/id_notes_vitals';

// START ROUTES FOR CRUD_TEMP
$route['crud_temp'] = 'crud/c_crud_temp';

//////////////////// START ROUTES FOR DEPARTEMENTS ////////////////////
$route['departements'] = 'rs/departements/c_departements';
//////////////////// END ROUTES FOR DEPARTEMENTS ////////////////////

// Start routes for ref_rs
$route['ref_rs/(:any)']    = 'global/c_referensi/$1';
